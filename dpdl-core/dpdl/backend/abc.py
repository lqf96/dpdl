from abc import ABC, abstractproperty
import time

from .consts import LOG_TEXT, LOG_EPOCH

class Backend(ABC):
    """ Abstract DPDL training and applying backend class. """
    def __init__(self, params):
        ## Backend parameters
        self.params = params
    @abstractproperty
    def roles_mapping(self):
        """ Role name to class or class path mapping. """
        raise NotImplementedError()
    @abstractproperty
    def training_roles(self):
        """ List of training roles. """
        raise NotImplementedError()
    @abstractproperty
    def applying_roles(self):
        """ List of applying roles. """
        raise NotImplementedError()
    def roles_for_member(self, member_id, params={}):
        """
        Allocate roles for member.

        :param member_id: Member ID.
        :param params: Parameters sent from the member.
        :returns: A list of roles.
        """
        raise NotImplementedError()
    def init_topology_for_member(self, n_workers, member_id, params={}):
        """
        Determine initial topology for member.

        :param n_workers: Number of workers.
        :param member_id: Member ID.
        :param params: Parameters sent from the member.
        :returns: List of member IDs for the member to connect.
        """
        raise NotImplementedError()

class Role(ABC):
    """ Abstract training and applying role class. """
    pass

class Primitives(ABC):
    """ Abstract training primitives class. """
    pass

class LoggingPrimitives(Primitives):
    def __init__(self):
        """ Initializing logging primitives. """
        ## Log queue
        self._log_queue = []
    def log(self, log_type, data):
        """
        Log data in the program.

        :param log_type: Log type.
        :param data: Data to log.
        """
        self._log_queue.append((log_type, data))
    def log_text(self, text):
        """
        Log text in the program.

        :param text: Text to log.
        """
        return self.log(LOG_TEXT, text)
    def log_epoch(self, global_epoch, local_epoch, **kwargs):
        """
        Log global epoch, local epoch number and loss.

        :param global_epoch: Global epoch number.
        :param local_epoch: Local epoch number.
        """
        return self.log(LOG_EPOCH, (
            time.time(),
            global_epoch,
            local_epoch,
            kwargs
        ))
    async def flush(self):
        """
        Flush local logs to the remote leader node.

        :returns: A future resolved when all logs are sent.
        """
        raise NotImplementedError()

class AveragingPrimitives(Primitives):
    async def average(self, network, global_epoch):
        """
        Abstract 'averaging' primitive.

        :param network: PyTorch network instance.
        :param global_epoch: Global epoch number.
        :returns: A future resolved when the 'averaging' process completes.
        """
        raise NotImplementedError()