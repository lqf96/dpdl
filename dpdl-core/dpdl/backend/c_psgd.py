import asyncio, logging
import numpy as np, torch

from dpdl.utils.io import filter_recv, of_msg_type, as_torch_tensor, as_numpy_array
from .abc import Backend, Role, LoggingPrimitives, AveragingPrimitives
from .consts import MSG_LOG, LOG_TEXT, LOG_EPOCH, MSG_CPSGD_MASTER_PARAM, MSG_CPSGD_SLAVE_PARAM

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

class CPSGDMaster(object):
    """ C-PSGD master role. """
    def __init__(self, group):
        super(CPSGDMaster, self).__init__()
        ## Group reference
        self._group = group
        # Subscribe log from merged Rx
        s = group.merged_rx.filter(of_msg_type(MSG_LOG))
        s.subscribe(on_next=self._handle_log)
    def _handle_log(self, log_msg):
        member_id = log_msg["member_id"]
        task_config = self._group.task_config
        n_global_epochs = task_config["n_global_epochs"]
        n_local_epochs = task_config["n_local_epochs"]
        # Training statistics
        training_stats = self._group.backend_config.get("training_stats")
        # Log to the console
        for log_type, log_data in log_msg["logs"]:
            # Text log
            if log_type==LOG_TEXT:
                _logger.debug("[#{}] [Text] {}".format(member_id, log_data))
            # Epoch log
            elif log_type==LOG_EPOCH:
                _logger.debug("[#{}] [Epoch] ({}, {}/{}, {}/{}) {}".format(
                    member_id, log_data[0], log_data[1]+1, task_config["n_global_epochs"],
                    log_data[2]+1, task_config["n_local_epochs"], log_data[3]
                ))
                # Save training statistics
                if training_stats!=None and log_data[2]==0:
                    epoch_loss = training_stats.setdefault("epoch_loss", {})
                    epoch_time = training_stats.setdefault("epoch_time", {})
                    # Create statistics array for given member
                    epoch_loss_member = epoch_loss.get(member_id)
                    epoch_time_member = epoch_time.get(member_id)
                    if epoch_loss_member==None:
                        epoch_loss_member = epoch_loss[member_id] = [0]*n_global_epochs
                        epoch_time_member = epoch_time[member_id] = [0]*n_global_epochs
                    # Save statistics
                    epoch_loss_member[log_data[1]] = log_data[3].get("loss", 0)
                    epoch_time_member[log_data[1]] = log_data[0]
    async def train(self, task, _):
        group = self._group
        n_workers = group.n_workers
        device = group.device
        # Send shared initial state to prevent bad behavior
        if group.backend_config.get("share_init_params"):
            # Get PyTorch params and convert to NumPy array
            params = as_numpy_array(task.network.state_dict())
            # Send initial parameters
            group.merged_tx.on_next({
                "msg_type": MSG_CPSGD_MASTER_PARAM,
                "member_id": 0,
                "global_epoch": -1,
                "params": params
            })
            _logger.debug("Shared initial state sent to members.")
        # Training loop
        for global_epoch in range(task.params["n_global_epochs"]):
            wait_futures = []
            # Receive parameters from other members
            for i in range(1, n_workers):
                member = group.member_by_id(i)
                wait_futures.append(filter_recv(member.rx, {
                    "msg_type": MSG_CPSGD_SLAVE_PARAM,
                    "member_id": member.member_id,
                    "global_epoch": global_epoch
                }))
            msgs = await asyncio.gather(*wait_futures)
            # Averaging
            averaged_params = {}
            for msg in msgs:
                msg_params = msg["params"]
                for key, param in msg_params.items():
                    # Create new parameter
                    if key not in averaged_params:
                        averaged_params[key] = np.zeros_like(param)
                    averaged_params[key] += param/(n_workers-1)
            # Send parameters to other members
            group.merged_tx.on_next({
                "msg_type": MSG_CPSGD_MASTER_PARAM,
                "member_id": 0,
                "global_epoch": global_epoch,
                "params": averaged_params
            })
            # Convert back to PyTorch tensor
            params = as_torch_tensor(averaged_params, device)
            # Set PyTorch parameters
            task.network.load_state_dict(params)
    async def apply(self, task, inputs):
        return await task.apply(inputs)

class CPSGDSlave(LoggingPrimitives, AveragingPrimitives):
    """ C-PSGD slave role. """
    def __init__(self, group):
        super(CPSGDSlave, self).__init__()
        ## Group reference
        self._group = group
    async def flush(self):
        # Get master member
        master = self._group.member_by_id(0)
        # Send logs to master node
        master.tx.on_next({
            "msg_type": MSG_LOG,
            "member_id": self._group.self_member_id,
            "logs": self._log_queue
        })
        # Clear logs
        self._log_queue.clear()
    async def average(self, network, global_epoch):
        group = self._group
        # Get master member
        master = group.member_by_id(0)
        # Get PyTorch params and convert to NumPy array
        params = as_numpy_array(network.state_dict())
        # Send parameters to master member
        master.tx.on_next({
            "msg_type": MSG_CPSGD_SLAVE_PARAM,
            "member_id": group.self_member_id,
            "global_epoch": global_epoch,
            "params": params
        })
        # Receive parameters from master member
        master_msg = await filter_recv(master.rx, {
            "msg_type": MSG_CPSGD_MASTER_PARAM,
            "member_id": 0,
            "global_epoch": global_epoch
        })
        # Convert back to PyTorch tensor
        params = as_torch_tensor(master_msg["params"], group.device)
        # Set PyTorch params
        network.load_state_dict(params)
    async def train(self, task, loader):
        group = self._group
        # Receive initial parameters from master member
        if group.backend_config.get("share_init_params"):
            master = group.member_by_id(0)
            device = group.device
            _logger.debug("Waiting for shared initial state.")
            # Get initial parameters
            master_msg = await filter_recv(master.rx, {
                "msg_type": MSG_CPSGD_MASTER_PARAM,
                "member_id": 0,
                "global_epoch": -1
            })
            _logger.debug("Shared initial state received from master.")
            # Convert back to PyTorch tensor
            params = as_torch_tensor(master_msg["params"], device)
            # Set PyTorch params
            task.network.load_state_dict(params)
        # Train on the task
        return await task.train(loader, self)

class CPSGDBackend(Backend):
    """ C-PSGD training and applying backend. """
    def roles_for_member(self, member_id, params):
        # Member #0 is master, other members are slave
        return ["master"] if member_id==0 else ["slave"]
    def init_topology_for_member(self, n_workers, member_id, params):
        # All members only need to connect to master
        return []
    ## Roles mapping
    roles_mapping = {"slave": CPSGDSlave, "master": CPSGDMaster}
    ## Training roles
    training_roles = ["slave", "master"]
    ## Applying roles
    applying_roles = ["master"]
