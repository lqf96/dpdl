# ===== Log Types =====
## Text log
LOG_TEXT = "log_text"
## Epoch log
LOG_EPOCH = "log_epoch"

# ===== Message Types =====
## Log message
MSG_LOG = "log"

## C-PSGD parameter update
MSG_CPSGD_SLAVE_PARAM = "cpsgd_slave_param"
## C-PSGD master parameter update
MSG_CPSGD_MASTER_PARAM = "cpsgd_master_param"

## D-PSGD parameter
MSG_DPSGD_PARAM = "dpsgd_param"

## Dynamic D-PSGD link statistics
MSG_DYNDPSGD_LINK_STAT = "dyndpsgd_link_stat"
## Dynamic D-PSGD update neighbors
MSG_DYNDPSGD_UPDATE_NEIGHBORS = "dyndpsgd_update_neighbors"
