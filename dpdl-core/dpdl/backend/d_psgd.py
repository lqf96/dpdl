import asyncio, logging, math

from dpdl.utils.io import filter_recv, of_msg_type, as_numpy_array, as_torch_tensor
from .abc import Backend, LoggingPrimitives, AveragingPrimitives
from .consts import MSG_LOG, LOG_TEXT, LOG_EPOCH, MSG_DPSGD_PARAM

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

class DPSGDMaster(object):
    """ D-PSGD master role. """
    def __init__(self, group):
        super(DPSGDMaster, self).__init__()
        ## Group reference
        self._group = group
        # Subscribe log from merged Rx
        s = group.merged_rx.filter(of_msg_type(MSG_LOG))
        s.subscribe(on_next=self._handle_log)
    def _handle_log(self, log_msg):
        member_id = log_msg["member_id"]
        task_config = self._group.task_config
        n_global_epochs = task_config["n_global_epochs"]
        n_local_epochs = task_config["n_local_epochs"]
        # Training statistics
        training_stats = self._group.backend_config.get("training_stats")
        # Log to the console
        for log_type, log_data in log_msg["logs"]:
            # Text log
            if log_type==LOG_TEXT:
                _logger.debug("[#{}] [Text] {}".format(member_id, log_data))
            # Epoch log
            elif log_type==LOG_EPOCH:
                _logger.debug("[#{}] [Epoch] ({}, {}/{}, {}/{}) {}".format(
                    member_id, log_data[0], log_data[1]+1, task_config["n_global_epochs"],
                    log_data[2]+1, task_config["n_local_epochs"], log_data[3]
                ))
                # Save training statistics
                if training_stats!=None and log_data[2]==0:
                    epoch_loss = training_stats.setdefault("epoch_loss", {})
                    epoch_time = training_stats.setdefault("epoch_time", {})
                    # Create statistics array for given member
                    epoch_loss_member = epoch_loss.get(member_id)
                    epoch_time_member = epoch_time.get(member_id)
                    if epoch_loss_member==None:
                        epoch_loss_member = epoch_loss[member_id] = [0]*n_global_epochs
                        epoch_time_member = epoch_time[member_id] = [0]*n_global_epochs
                    # Save statistics
                    epoch_loss_member[log_data[1]] = log_data[3].get("loss", 0)
                    epoch_time_member[log_data[1]] = log_data[0]

class DPSGDWorker(LoggingPrimitives, AveragingPrimitives):
    """ D-PSGD worker role. """
    def __init__(self, group):
        super(DPSGDWorker, self).__init__()
        ## Group reference
        self._group = group
        ## Neighbor members
        self._neighbor_ids = group.backend_config["neighbor_ids"]
    async def flush(self):
        group = self._group
        # Get master member
        master = group.member_by_id(0)
        # Send logs to master node
        master.tx.on_next({
            "msg_type": MSG_LOG,
            "member_id": group.self_member_id,
            "logs": self._log_queue
        })
        # Clear logs
        self._log_queue.clear()
    async def average(self, network, global_epoch):
        group = self._group
        # Get PyTorch params
        params = as_numpy_array(network.state_dict())
        # Get neighbor members
        neighbor_ids = self._neighbor_ids
        # Send parameters to neighbors
        for member_id in neighbor_ids:
            neighbor_member = group.member_by_id(member_id)
            neighbor_member.tx.on_next({
                "msg_type": MSG_DPSGD_PARAM,
                "global_epoch": global_epoch,
                "member_id": group.self_member_id,
                "params": params
            })
        # Receive parameters from neighbors
        param_msg_futures = []
        for member_id in neighbor_ids:
            neighbor_member = group.member_by_id(member_id)
            param_msg_futures.append(filter_recv(neighbor_member.rx, {
                "msg_type": MSG_DPSGD_PARAM,
                "global_epoch": global_epoch,
                "member_id": neighbor_member.member_id,
            }))
        msgs = await asyncio.gather(*param_msg_futures)
        # Averaging
        n_neighbors = len(neighbor_ids)
        for key in params:
            params[key] /= n_neighbors+1
        for msg in msgs:
            msg_params = msg["params"]
            for key, param in msg_params.items():
                params[key] += param/(n_neighbors+1)
        # Convert to PyTorch tensor
        params = as_torch_tensor(params, group.device)
        # Set PyTorch params
        network.load_state_dict(params)
    async def train(self, task, loader):
        group = self._group
        # Master node, send initial parameters to client
        if group.self_member_id==0:
            # Get PyTorch params and convert to NumPy array
            params = as_numpy_array(task.network.state_dict())
            # Send parameters to members
            group.merged_tx.on_next({
                "msg_type": MSG_DPSGD_PARAM,
                "member_id": 0,
                "global_epoch": -1,
                "params": params
            })
        # Other workers, receive initial parameters
        else:
            master = group.member_by_id(0)
            master_msg = await filter_recv(master.rx, {
                "msg_type": MSG_DPSGD_PARAM,
                "member_id": 0,
                "global_epoch": -1
            })
            # Convert to PyTorch tensor
            params = as_torch_tensor(master_msg["params"], group.device)
            # Set PyTorch params
            task.network.load_state_dict(params)
        # Train on the task
        return await task.train(loader, self)
    async def apply(self, task, inputs):
        # Apply to the task
        return await task.apply(inputs)

class DPSGDBackend(Backend):
    """ D-PSGD training and applying backend. """
    def __init__(self, params):
        super(DPSGDBackend, self).__init__(params)
        ## Neighbor table
        self._neighbor_table = None
    def _get_neighbor_table(self, n_workers):
        n_shortcut_neighbors = self.params.get("n_shortcut_neighbors", 0)
        interval = n_workers/(n_shortcut_neighbors+1)
        # Neighbor table
        neighbor_table = []
        for _ in range(n_workers):
            neighbor_table.append(set())
        # Neighbors
        for member_id in range(n_workers):
            current_neighbors = neighbor_table[member_id]
            # Add adjacent neighbors
            prev_id = n_workers-1 if member_id==0 else member_id-1
            next_id = 0 if member_id==n_workers-1 else member_id+1
            current_neighbors.add(prev_id)
            current_neighbors.add(next_id)
            # Add shortcut neighbors
            for i in range(n_shortcut_neighbors):
                next_shortcut_id = math.floor(member_id+interval*i)%n_workers
                current_neighbors.add(next_shortcut_id)
                neighbor_table[next_shortcut_id].add(member_id)
        return neighbor_table
    def roles_for_member(self, member_id, params):
        # All nodes are worker
        roles = ["worker"]
        # Master for #0
        if member_id==0:
            roles.append("master")
        return roles
    def init_topology_for_member(self, n_workers, member_id, params):
        # Get neighbor table
        neighbor_table = self._neighbor_table
        if not neighbor_table:
            neighbor_table = self._neighbor_table = self._get_neighbor_table(n_workers)
        return list(neighbor_table[member_id])
    ## Roles mapping
    roles_mapping = {"worker": DPSGDWorker, "master": DPSGDMaster}
    ## Training roles
    training_roles = ["worker"]
    ## Applying roles
    applying_roles = ["worker"]
