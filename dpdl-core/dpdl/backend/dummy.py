import logging

from .abc import Backend, LoggingPrimitives, AveragingPrimitives
from .consts import LOG_TEXT, LOG_EPOCH

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

class DummyWorker(LoggingPrimitives, AveragingPrimitives):
    """ Dummy worker role. """
    def __init__(self, group):
        super(DummyWorker, self).__init__()
        ## Group reference
        self._group = group
    async def flush(self):
        task_config = self._group.task_config
        n_global_epochs = task_config["n_global_epochs"]
        n_local_epochs = task_config["n_local_epochs"]
        # Training statistics
        training_stats = self._group.backend_config.get("training_stats")
        # Log to the console
        for log_type, log_data in self._log_queue:
            # Text log
            if log_type==LOG_TEXT:
                _logger.debug("[Text] {}".format(log_data))
            # Epoch log
            elif log_type==LOG_EPOCH:
                _logger.debug("[Epoch] ({}, {}/{}, {}/{}) {}".format(
                    log_data[0], log_data[1]+1, n_global_epochs,
                    log_data[2]+1, n_local_epochs, log_data[3]
                ))
                # Save training statistics
                if training_stats!=None and log_data[2]==0:
                    epoch_loss = training_stats.setdefault("epoch_loss", {})
                    epoch_time = training_stats.setdefault("epoch_time", {})
                    # Create statistics array for member #0
                    epoch_loss_0 = epoch_loss.get(0)
                    epoch_time_0 = epoch_time.get(0)
                    if epoch_loss_0==None:
                        epoch_loss_0 = epoch_loss[0] = [0]*n_global_epochs
                        epoch_time_0 = epoch_time[0] = [0]*n_global_epochs
                    # Save statistics
                    epoch_loss_0[log_data[1]] = log_data[3].get("loss", 0)
                    epoch_time_0[log_data[1]] = log_data[0]
        # Clear queue
        self._log_queue.clear()
    async def average(self, network, global_epoch):
        pass
    async def train(self, task, loader):
        # Train on the task
        await task.train(loader, self)
    async def apply(self, task, inputs):
        # Apply to the task
        return await task.apply(inputs)

class DummyBackend(Backend):
    """ Dummy training and applying backend. """
    def roles_for_member(self, member_id, params):
        # Master for the only worker
        return ["master"]
    def init_topology_for_member(self, n_workers, member_id, params):
        # No one to connect
        return []
    ## Roles mapping
    roles_mapping = {"master": DummyWorker}
    ## Training roles
    training_roles = ["master"]
    ## Applying roles
    applying_roles = ["master"]
    