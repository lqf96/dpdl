from .abc import Backend
from .d_psgd import DPSGDLeader, DPSGDWorker

class DynDPSGDMaster(DPSGDLeader):
    pass

class DynDPSGDWorker(DPSGDWorker):
    pass

class DynDPSGDBackend(Backend):
    """ Dynamic D-PSGD training and applying backend. """
    ## Roles mapping
    roles_mapping = {"worker": DPSGDWorker, "leader": DPSGDLeader}
    ## Training roles
    training_roles = ["worker"]
    ## Applying roles
    applying_roles = ["worker"]