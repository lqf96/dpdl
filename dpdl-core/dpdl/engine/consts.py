## Publish training task
MSG_DPDL_PUBLISH_TASK = "publish_task"
## Accept training task
MSG_DPDL_ACCEPT_TASK = "accept_task"
## Worker accepted
MSG_DPDL_WORKER_ACCEPTED = "worker_accepted"
## Worker rejected
MSG_DPDL_WORKER_REJECTED = "worker_rejected"
## Neighbor hello
MSG_DPDL_NEIGHBOR_HELLO = "neighbor_hello"
## Neighbor hello reply
MSG_DPDL_NEIGHBOR_REPLY = "neighbor_reply"
## Training ready
MSG_DPDL_TRAINING_READY = "training_ready"
## Training start
MSG_DPDL_TRAINING_START = "training_start"
## Training end
MSG_DPDL_TRAINING_END = "training_end"
## Find peer of group
MSG_DPDL_FIND_PEER = "find_peer"
## Peer of group found
MSG_DPDL_PEER_FOUND = "peer_found"
## Apply request
MSG_DPDL_APPLY_REQ = "apply_req"
## Apply response
MSG_DPDL_APPLY_RESP = "apply_resp"

## Maximum group ID
MAX_GROUP_ID = 65535
## Maximum apply request ID
MAX_APPLY_REQ_ID = 65535
