import asyncio, random, functools, logging

from dpdl.utils.loader import as_class, as_class_path
from dpdl.utils.io import subset_of, filter_recv, of_msg_type, future_ensured
from .group import Group
from .member import Member
from .consts import *

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

class Engine(object):
    """ DPDL training and applying engine class. """
    def __init__(self, network):
        ## P2P network reference
        self._network = network
        ## Groups mapping
        self._groups = {}
    @future_ensured
    async def _handle_neighbor_hello(self, group, msg):
        # Get peer
        peer = self._network.get_peer(msg["_peer_id"])
        # Create and add member
        member_id = msg["member_id"]
        _logger.debug("Neighbor hello from member #{}".format(member_id))
        if not group.member_by_id(member_id):
            group.add_member(Member(
                peer=peer,
                group_id=msg["group_id"],
                member_id=member_id,
                roles=msg["roles"]
            ))
            _logger.debug("New member #{} created by worker".format(member_id))
        # Self member
        self_member = group.member_by_id(group.self_member_id)
        # Respond with neighbor reply message
        peer.connection.tx.on_next({
            "msg_type": MSG_DPDL_NEIGHBOR_REPLY,
            "group_id": group.group_id,
            "member_id": group.self_member_id,
            "roles": self_member.roles
        })
    def _reject_excessive_peers(self, group, msg):
        """
        Reject request for excessive peers.
        """
        # Get peer
        peer = self._network.get_peer(msg["_peer_id"])
        group_id = group.group_id
        # Send worker rejected message
        peer.connection.tx.on_next({
            "msg_type": MSG_DPDL_WORKER_REJECTED,
            "group_id": group.group_id
        })
        _logger.debug("Excessive peer for group #{} rejected".format(group_id))
    async def publish_task(self, backend_class, backend_config, n_workers,
        task_class, task_config, dataset_class, dataset_config):
        """
        Publish training task, become leader of the group and find workers for the task.

        :param backend_class: Backend class.
        :param backend_config: Backend confifuration.
        :param n_workers: Number of workers needed.
        :param task_class: Task class.
        :param task_config: Task confifuration.
        :param dataset_class: Dataset class.
        :param dataset_config: Dataset confifuration.
        """
        network = self._network
        loop = asyncio.get_event_loop()
        # Create backend
        backend_class = as_class(backend_class)
        backend = backend_class(backend_config)
        # Create new group
        group_id = random.randint(0, MAX_GROUP_ID)
        group = self._groups[group_id] = Group(
            network=network,
            group_id=group_id,
            n_workers=n_workers,
            self_member_id=0,
            backend=backend,
            backend_class=backend_class,
            task_class=as_class(task_class),
            dataset_class=as_class(dataset_class),
            backend_config=backend_config,
            task_config=task_config,
            dataset_config=dataset_config
        )
        _logger.debug("New group #{} created".format(group_id))
        # Allocate roles for self
        leader_roles = backend.roles_for_member(0, backend_config)
        # Create member for self
        leader_member = Member(
            peer=self._network.self_peer,
            group_id=group_id,
            member_id=0,
            roles=leader_roles
        )
        group.add_member(leader_member)
        _logger.debug("Leader member added for group")
        # Prepare for accepting peers
        member_id = 1
        # Handle new members
        @future_ensured
        async def handle_new_members(msg):
            nonlocal member_id
            # Update member ID
            new_member_id = member_id
            member_id += 1
            # Allocate roles for member
            new_member_roles = backend.roles_for_member(new_member_id, msg)
            # Get peer
            new_member_peer = network.get_peer(msg["_peer_id"])
            # Create member for peer
            new_member = Member(
                peer=new_member_peer,
                group_id=group_id,
                member_id=new_member_id,
                roles=new_member_roles
            )
            group.add_member(new_member)
            _logger.debug("New member #{} created by master".format(new_member_id))
            # Determine topology for member
            neighbor_ids = backend.init_topology_for_member(n_workers, new_member_id, msg)
            # Get Multiaddr and PeerId for neighbors
            neighbor_peers = {}
            for neighbor_id in neighbor_ids:
                # Ignore temporarily unknown peers
                if neighbor_id>=new_member_id:
                    continue
                # Add member ID to neighbor mapping
                neighbor_peer = group.member_by_id(neighbor_id).peer
                neighbor_peer_id = await neighbor_peer.get_id()
                neighbor_addr = await neighbor_peer.get_address()
                neighbor_peers[neighbor_id] = [neighbor_peer_id, neighbor_addr]
            # Send worker accepted message
            new_member_peer.connection.tx.on_next({
                "msg_type": MSG_DPDL_WORKER_ACCEPTED,
                "group_id": group_id,
                "member_id": new_member_id,
                "roles": new_member_roles,
                "leader_roles": leader_roles,
                "neighbor_ids": neighbor_ids,
                "neighbor_peers": neighbor_peers,
                "backend_config": backend_config,
                "task_config": task_config,
                "dataset_config": dataset_config
            })
            _logger.debug("Worker accepted message sent to member #{}".format(new_member_id))
        # Accept peers
        if n_workers>1:
            s = network.merged_rx.filter(lambda msg: subset_of({
                "msg_type": MSG_DPDL_ACCEPT_TASK,
                "group_id": group_id
            }, msg))
            s = s.take(n_workers-1)
            s = s.map(handle_new_members)
            accept_peers_future = s.to_future()
        # Handle neighbor hello message
        s = network.merged_rx.filter(of_msg_type(MSG_DPDL_NEIGHBOR_HELLO))
        s.subscribe(on_next=lambda msg: self._handle_neighbor_hello(group, msg))
        # Broadcast publish task message
        network.broadcast_channel.tx.on_next({
            "msg_type": MSG_DPDL_PUBLISH_TASK,
            "backend_class": as_class_path(backend_class),
            "task_class": as_class_path(task_class),
            "dataset_class": as_class_path(dataset_class),
            "group_id": group_id,
            "n_workers": n_workers,
            "leader_peer_id": await network.self_peer.get_id(),
            "leader_addr": await network.self_peer.get_address()
        })
        _logger.debug("Publish task message broadcasted")
        # Wait for accepting peers
        if n_workers>1:
            await accept_peers_future
        # Determine topology for master
        master_neighbor_ids = backend.init_topology_for_member(n_workers, 0, backend_config)
        group.backend_config["neighbor_ids"] = master_neighbor_ids
        # Reject excessive peers
        s = network.merged_rx.filter(lambda msg: subset_of({
            "msg_type": MSG_DPDL_ACCEPT_TASK,
            "group_id": group_id
        }, msg))
        s.subscribe(on_next=lambda msg: self._reject_excessive_peers(group, msg))
        # Wait for training ready messages
        training_ready_futures = []
        for member_id in range(1, n_workers):
            member = group.member_by_id(member_id)
            training_ready_futures.append(filter_recv(
                member.rx,
                MSG_DPDL_TRAINING_READY
            ))
        if training_ready_futures:
            await asyncio.gather(*training_ready_futures)
            _logger.debug("Training ready message received from all members")
        # Broadcast training start message
        group.merged_tx.on_next({
            "msg_type": MSG_DPDL_TRAINING_START
        })
        # Start training
        await group.training_phase()
        # Wait for training end messages
        training_end_futures = []
        for member_id in range(1, n_workers):
            member = group.member_by_id(member_id)
            training_ready_futures.append(filter_recv(
                member.rx,
                MSG_DPDL_TRAINING_READY
            ))
        if training_end_futures:
            await asyncio.gather(*training_end_futures)
            _logger.debug("Training end message received from all members")
        # Start applying
        asyncio.ensure_future(group.applying_phase())
        # Return group ID
        return group_id
    async def wait_accept_task(self):
        """
        Wait and accept task, then executes training tasks and wait for applying requests.
        """
        network = self._network
        loop = asyncio.get_event_loop()
        # Get publish task message
        publish_task_msg = await filter_recv(network.broadcast_channel.rx, {
            "msg_type": MSG_DPDL_PUBLISH_TASK
        })
        # Group ID
        group_id = publish_task_msg["group_id"]
        n_workers = publish_task_msg["n_workers"]
        _logger.debug("Publish task message for group #{} received".format(group_id))
        # Get backend, task and dataset class
        backend_class = as_class(publish_task_msg["backend_class"])
        task_class = as_class(publish_task_msg["task_class"])
        dataset_class = as_class(publish_task_msg["dataset_class"])
        # Connect to leader peer
        leader = network.get_peer(publish_task_msg["leader_peer_id"])
        if not leader:
            leader = await network.connect(publish_task_msg["leader_addr"])
        connection = leader.connection
        _logger.debug("Connected to leader")
        # Send accept task message
        connection.tx.on_next({
            "msg_type": MSG_DPDL_ACCEPT_TASK,
            "group_id": group_id
        })
        # Response from leader
        worker_reply_msg = await filter_recv(connection.rx, [
            MSG_DPDL_WORKER_ACCEPTED,
            MSG_DPDL_WORKER_REJECTED
        ])
        # Worker rejected
        if worker_reply_msg["msg_type"]!=MSG_DPDL_WORKER_ACCEPTED:
            _logger.debug("Worker rejected")
            return
        # Create backend
        backend_config = worker_reply_msg["backend_config"]
        task_config = worker_reply_msg["task_config"]
        dataset_config = worker_reply_msg["dataset_config"]
        backend = backend_class(backend_config)
        # Create new group
        member_id = worker_reply_msg["member_id"]
        group = self._groups[group_id] = Group(
            network=network,
            group_id=group_id,
            n_workers=n_workers,
            self_member_id=member_id,
            backend=backend,
            backend_class=backend_class,
            task_class=task_class,
            dataset_class=dataset_class,
            backend_config=backend_config,
            task_config=task_config,
            dataset_config=dataset_config
        )
        _logger.debug("Worker accepted as member #{} of group".format(member_id))
        # Create member for self
        self_member = Member(
            peer=self._network.self_peer,
            group_id=group_id,
            member_id=member_id,
            roles=worker_reply_msg["roles"]
        )
        group.add_member(self_member)
        _logger.debug("Self member added for group")
        # Create member for leader
        leader_member = Member(
            peer=leader,
            group_id=group_id,
            member_id=0,
            roles=worker_reply_msg["leader_roles"]
        )
        group.add_member(leader_member)
        _logger.debug("Leader member added for group")
        # Handle neighbor hello message
        s = network.merged_rx.filter(of_msg_type(MSG_DPDL_NEIGHBOR_HELLO))
        s.subscribe(on_next=lambda msg: self._handle_neighbor_hello(group, msg))
        # Save neighbor IDs in backend configuration
        neighbor_ids = group.backend_config["neighbor_ids"] = worker_reply_msg["neighbor_ids"]
        # Connect to neighbors
        print(neighbor_ids)
        for neighbor_id in neighbor_ids:
            # Currently unknown member
            if neighbor_id>=member_id:
                continue
            # Neighbor addresses
            neighbor_peer_id, neighbor_addr = worker_reply_msg["neighbor_peers"][neighbor_id]
            # Get neighbor peer
            neighbor_peer = network.get_peer(neighbor_peer_id)
            if not neighbor_peer:
                neighbor_peer = await network.connect(neighbor_addr)
            # Send neighbor hello message to peer
            neighbor_conn = neighbor_peer.connection
            neighbor_conn.tx.on_next({
                "msg_type": MSG_DPDL_NEIGHBOR_HELLO,
                "group_id": group_id,
                "member_id": member_id,
                "roles": self_member.roles
            })
            _logger.debug("Neighbor hello message sent to member #{}".format(neighbor_id))
            # Receive neighbor hello message from peer
            resp = await filter_recv(neighbor_conn.rx, {
                "msg_type": MSG_DPDL_NEIGHBOR_REPLY,
                "group_id": group_id,
                "member_id": neighbor_id
            })
            # Create member for neighbor
            if not group.member_by_id(neighbor_id):
                group.add_member(Member(
                    peer=neighbor_peer,
                    group_id=group_id,
                    member_id=neighbor_id,
                    roles=resp["roles"]
                ))
            _logger.debug("New member #{} created by worker".format(neighbor_id))
        # Send training ready message to master
        leader_member.tx.on_next({
            "msg_type": MSG_DPDL_TRAINING_READY
        })
        _logger.debug("Worker #{} ready for training".format(member_id))
        # Start training
        await group.training_phase()
        # Start applying
        asyncio.ensure_future(group.applying_phase())
    async def apply(self, group_id, inputs):
        network = self._network
        # Member of given group
        group = self._groups.get(group_id)
        if group:
            result = await group.apply(inputs)
            if result is not None:
                return result
            # Not an applying role of the group, try to find someone who is
            backend = group.backend
            apply_peer = None
            for member in group.iter_members():
                for role in member.roles:
                    if role in backend.applying_roles:
                        apply_peer = member.peer
                        break
                if apply_member:
                    break
        # Fall back to broadcast to find peer of the group
        if not group or not apply_peer:
            # Broadcast find peer information
            network.broadcast_channel.tx.on_next({
                "msg_type": MSG_DPDL_FIND_PEER,
                "group_id": group_id,
                "addr": self._network.self_peer.address
            })
            # Wait for response
            find_peer_resp = await filter_recv(network.merged_rx, {
                "msg_type": MSG_DPDL_PEER_FOUND,
                "group_id": group_id
            })
            input_req_id = random.randint(0, MAX_APPLY_REQ_ID)
        # Send input to peer
        apply_peer = network.get_peer(resp["_peer_id"])
        peer.connection.tx.on_next({
            "msg_type": MSG_DPDL_APPLY_REQ,
            "inputs": inputs,
            "group_id": group_id,
            "req_id": input_req_id
        })
        # Wait for result
        apply_resp = await filter_recv(peer.connection.rx, {
            "msg_type": MSG_DPDL_APPLY_RESP,
            "group_id": group_id,
            "req_id": input_req_id
        })
        return apply_resp["result"]
