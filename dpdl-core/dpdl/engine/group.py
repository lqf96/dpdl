import asyncio, logging
import torch
from torch.utils.data import DataLoader
from rx.subjects import Subject, ReplaySubject

from dpdl.utils.io import subset_of, filter_recv, future_ensured
from .consts import *

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

class Group(object):
    def __init__(self, network, group_id, n_workers, self_member_id,
        backend, backend_class, task_class, dataset_class,
        backend_config, task_config, dataset_config):
        """
        Initialize a new group.
        """
        ## Group ID
        self.group_id = group_id
        ## Number of workers
        self.n_workers = n_workers
        ## Self member ID
        self.self_member_id = self_member_id
        ## Backend
        self.backend = backend
        ## Backend class
        self.backend_class = backend_class
        ## Backend config
        self.backend_config = backend_config
        ## Task class
        self.task_class = task_class
        ## Task config
        self.task_config = task_config
        ## Dataset class
        self.dataset_class = dataset_class
        ## Dataset config
        self.dataset_config = dataset_config
        ## Device used for training
        self.device = None
        ## Merged Rx
        self.merged_rx = Subject()
        ## Merged Tx
        self.merged_tx = Subject()
        ## Roles mapping
        self.roles = {}
        ## Task
        self._task = None
        ## Network reference
        self._network = network
        ## ID to member mapping
        self._id_member_mapping = {}
    @future_ensured
    async def _handle_find_peer(self, msg):
        # Get peer for network
        peer = self._network.get_peer(msg["_peer_id"])
        if not peer:
            peer = await self._network.connect(msg["addr"])
        # Respond with peer found message
        peer.connection.tx.on_next({
            "msg_type": MSG_DPDL_PEER_FOUND,
            "group_id": self.group_id
        })
    def iter_members(self):
        """ Iterate over all known members of the group. """
        return iter(self._id_member_mapping.values())
    def add_member(self, member):
        """
        Add a new member to the group.

        :param member: Member object.
        """
        member_id = member.member_id
        # Member already present
        if member_id in self._id_member_mapping:
            return
        # ID mapping
        self._id_member_mapping[member_id] = member
        # Message filter
        member_conn = member.peer.connection
        # Subscribe to group Rx
        s = member_conn.rx.filter(lambda msg: msg.get("group_id")==self.group_id)
        s.subscribe(self.merged_rx)
        # Subscribe to group Tx
        s = self.merged_tx.map(lambda msg: dict(msg, group_id=self.group_id))
        s.subscribe(member_conn.tx)
    def member_by_id(self, member_id):
        """
        Get member by its member ID.

        :param member_id: Member ID.
        """
        return self._id_member_mapping.get(member_id)
    async def training_phase(self):
        """ Training phase for the current task. """
        master = self.member_by_id(0)
        # Waiting for configuration from master
        if self.self_member_id!=0:
            await filter_recv(master.rx, {
                "msg_type": MSG_DPDL_TRAINING_START
            })
            _logger.debug("Training start message received from master")
        # Training role
        training_role = None
        # Device to use
        self.device = torch.device(self.backend_config.get("device", "cpu"))
        # Create roles
        roles = self.roles = {}
        self_peer = self.member_by_id(self.self_member_id)
        for role_name in self_peer.roles:
            # Create role
            roles[role_name] = role = self.backend.roles_mapping[role_name](self)
            _logger.debug("Role {} has been created".format(role_name))
            # Training role
            if not training_role and role_name in self.backend.training_roles:
                # Create dataset and data loader
                dataset = self.dataset_class(self.dataset_config)
                loader = DataLoader(
                    dataset=dataset,
                    batch_size=self.dataset_config["batch_size"],
                    shuffle=True
                )
                # Create task
                task = self._task = self.task_class(
                    params=self.task_config,
                    device=self.device
                )
                _logger.debug("Task instance created")
                # Task training process
                training_role = role
        # Start training role
        if training_role:
            _logger.debug("Training role started")
            await training_role.train(task, loader)
        # Send training end message to master
        if self.self_member_id!=0:
            master.tx.on_next({
                "msg_type": MSG_DPDL_TRAINING_END
            })
        _logger.debug("Training ended")
    async def applying_phase(self):
        """ Applying phase for the current task. """
        loop = asyncio.get_event_loop()
        # Handle find peer requests
        s = self._network.broadcast_channel.rx.filter(lambda msg: subset_of({
            "msg_type": MSG_DPDL_FIND_PEER,
            "group_id": self.group_id
        }, msg))
        s.subscribe(on_next=self._handle_find_peer)
        # Apply handling loop
        while True:
            # Receive request
            req = await filter_recv(self._network.merged_rx, {
                "msg_type": MSG_DPDL_APPLY_REQ,
                "group_id": self.group_id,
            })
            # Apply 
            result = await self.apply(req["inputs"])
            # Respond with result
            peer = self._network.get_peer(req["_peer_id"])
            peer.connection.tx.on_next({
                "msg_type": MSG_DPDL_APPLY_RESP,
                "group_id": self.group_id,
                "req_id": req["req_id"],
                "result": result
            })
    async def apply(self, inputs):
        """ Apply to the current task of the group. """
        for role_name, role in self.roles.items():
            # Belongs to applying role
            if role_name in self.backend.applying_roles:
                # Apply input to network
                return await role.apply(self._task, inputs)
        return None
