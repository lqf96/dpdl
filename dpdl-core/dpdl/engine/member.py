from rx.subjects import Subject, ReplaySubject

class Member(object):
    def __init__(self, peer, group_id, member_id, roles):
        ## Peer reference
        self.peer = peer
        ## Group ID
        self.roles = roles
        ## Group ID
        self.group_id = group_id
        ## Member ID
        self.member_id = member_id
        ## Roles
        self.roles = roles
        ## Tx
        tx = self.tx = Subject()
        ## Rx
        rx = self.rx = ReplaySubject(window=20000)
        # Connect Tx to peer Tx
        s = tx.map(lambda data: dict(data, group_id=self.group_id))
        s.subscribe(peer.connection.tx)
        # Connect Rx to peer Rx
        s = peer.connection.rx.filter(lambda data: data.get("group_id")==group_id)
        s.subscribe(rx)
