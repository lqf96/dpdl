import struct, math, asyncio
from asyncio import Future
from rx.subjects import Subject, ReplaySubject
import numpy as np, msgpack_numpy

from dpdl.utils.io import pack_data, unpack_data, future_ensured
from dpdl.utils.js_bridge import as_py_future, settler_callback
from dpdl.utils.reactive import PullStreamSource, PullStreamSink, msg_encoder, msg_decoder
from .rate_limit import RateLimitController

def serialize_np_types(obj):
    return msgpack_numpy.encode(obj)

def deserialize_np_types(obj):
    return msgpack_numpy.decode(obj)

## Common serializer mapping
COMMON_SERIALIZER_MAPPING = {
    np.ndarray: (0x40, serialize_np_types),
    np.bool_: (0x40, serialize_np_types),
    np.number: (0x40, serialize_np_types),
    complex: (0x40, serialize_np_types)
}

## Common deserializer mapping
COMMON_DESERIALIZER_MAPPING = {
    0x40: deserialize_np_types
}

class LoopbackConnection(object):
    def __init__(self):
        ## Message queue
        self._msg_queue = []
        ## Future queue
        self._future_queue = []
        ## Rx and Tx stream
        self.rx = self.tx = Subject()

class P2PConnection(object):
    def __init__(self, simulate_latency=0, simulate_bandwidth=0, rate_limit_simulator=None):
        ## Rx stream
        self.rx = ReplaySubject(window=20000)
        ## Tx stream
        self.tx = Subject()
        ## Rate limit controller
        self._rate_limit_controller = RateLimitController(
            simulate_latency,
            simulate_bandwidth,
            rate_limit_simulator
        )
        ## Current event loop
        self._loop = asyncio.get_event_loop()
    @future_ensured
    async def init_conn(self, g, conn):
        # Get pipe from and pipe to
        pipe_from = await g.get("pipe_from")
        pipe_to = await g.get("pipe_to")
        # Pull stream source and sink
        source = PullStreamSource()
        sink = PullStreamSink()
        # Connect Tx stream
        s = msg_encoder(self.tx, COMMON_SERIALIZER_MAPPING)
        s = self._rate_limit_controller.throttle(s)
        s.subscribe(source)
        await pipe_from([source.source, conn])
        # Connect Rx stream
        await pipe_to([conn, sink.sink])
        s = msg_decoder(sink, COMMON_DESERIALIZER_MAPPING)
        s.subscribe(self.rx)
        # Wait for some time to avoid loss of message
        await asyncio.sleep(0.5)

class BroadcastChannel(object):
    """ DPDL broadcast channel class. """
    def __init__(self, node, topic):
        ## Rx stream
        self.rx = Subject()
        ## Tx stream
        tx = self.tx = Subject()
        ## Underlying JS DPDL node
        self._node = node
        ## Libp2p PubSub topic name
        self._topic = topic
        ## PubSub instance cache
        self._pubsub = None
        # Subscribe to send messages
        tx.subscribe(on_next=self._handle_send_msg)
    async def start(self):
        # Subscribe to topic
        node_pubsub = self._pubsub = await self._node.get("pubsub")
        f = Future()
        await node_pubsub.call("subscribe", [
            self._topic,
            self._handle_recv_msg,
            settler_callback(f)
        ])
        await f
        # Wait for some time to avoid loss of messages
        await asyncio.sleep(0.5)
    @future_ensured
    async def _handle_recv_msg(self, pubsub_msg):
        # Extract and deserialize data
        raw_data = await pubsub_msg.get("data")
        msg = unpack_data(raw_data, COMMON_DESERIALIZER_MAPPING)
        # Push to Rx stream
        self.rx.on_next(msg)
    @future_ensured
    async def _handle_send_msg(self, msg):
        # Serialize data
        raw_data = pack_data(msg, COMMON_SERIALIZER_MAPPING)
        # Publish to topic
        f = Future()
        await self._pubsub.call("publish", [
            self._topic,
            raw_data,
            settler_callback(f)
        ])
        await f
