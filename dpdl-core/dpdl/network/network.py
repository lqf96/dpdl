import logging
from asyncio import Future
from rx.subjects import Subject, ReplaySubject

from dpdl.utils.js_bridge import settler_callback
from dpdl.utils.io import future_ensured, filter_recv
from .peer import Peer
from .connection import P2PConnection, BroadcastChannel
from .rate_limit import RateLimitController

## Bandwidth information
MSG_NET_BANDWIDTH_INFO = "bandwidth_info"

## DPDL protocol name
DPDL_PROTOCOL_NAME = "/dpdl/0.1.0"

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

class Network(object):
    def __init__(self, g, self_peer, rate_limit_simulator=None):
        """
        Initialize the P2P network.

        :param g: JS bridge global scope.
        :param self_peer: Self peer object.
        :param rate_limit_simulator: Rate limit simulator.
        """
        ## Self peer
        self.self_peer = self_peer
        ## Merged Rx stream
        self.merged_rx = Subject()
        ## Js bridge global scope
        self._g = g
        ## Rate limit simulator
        self._rate_limit_simulator = rate_limit_simulator
        ## Broadcast channel
        self._broadcast_channel = None
        ## Peer mapping
        self._peer_mapping = {}
    async def _init_node(self, bootstrap_addr):
        self_peer = self.self_peer
        dpdl_node_class = await self._g.get("DPDLNode")
        # Underlying JS DPDL node object
        node = self._node = await dpdl_node_class.new([
            self_peer.peer_info, bootstrap_addr
        ])
        # Register DPDL protocol
        await node.call("handle", [
            DPDL_PROTOCOL_NAME, self._handle_dpdl_protocol
        ])
        # Add self peer to peer mapping
        self_peer_id = await self_peer.get_id()
        self._peer_mapping[self_peer_id] = self_peer
    @future_ensured
    async def _handle_dpdl_protocol(self, _, js_conn):
        # Create peer
        peer = await self._create_peer(js_conn)
        # Receive latency and bandwidth information over the connection
        connection = peer.connection
        bandwidth_msg = await filter_recv(connection.rx, {
            "msg_type": MSG_NET_BANDWIDTH_INFO
        })
        _logger.debug("Bandwidth information received for connection")
        # Set latency, bandwidth and bandwidth simulator
        rate_limit_controller = connection._rate_limit_controller
        rate_limit_controller.simulate_latency = bandwidth_msg["latency"]
        rate_limit_controller.simulate_bandwidth = bandwidth_msg["bandwidth"]
        rate_limit_controller.rate_limit_simulator = self._rate_limit_simulator
    async def _create_peer(self, js_conn):
        # Get PeerInfo from connection
        peer_info_future = Future()
        await js_conn.call("getPeerInfo", [
            settler_callback(peer_info_future)
        ])
        peer_info = await peer_info_future
        # Create P2P connection and peer object
        connection = P2PConnection()
        await connection.init_conn(self._g, js_conn)
        peer = Peer(self._g, connection, peer_info)
        # Save into mapping
        peer_id = await peer.get_id()
        self._peer_mapping[peer_id] = peer
        _logger.debug("New peer created, ID: {}".format(peer_id))
        # Merge into merged Rx
        s = connection.rx.map(lambda data: dict(data, _peer_id=peer_id))
        s.subscribe(self.merged_rx)
        # Return peer
        return peer
    def get_peer(self, id):
        """
        Get peer by its Peer ID.

        :param id: Peer ID.
        """
        return self._peer_mapping.get(id)
    async def start(self, bootstrap_addr=None):
        # Initialize undelying node object
        await self._init_node(bootstrap_addr)
        # Start node
        start_future = Future()
        await self._node.call("start", [
            settler_callback(start_future)
        ])
        await start_future
        # Connect to bootstrap node
        if bootstrap_addr:
            connect_future = Future()
            await self._node.call("dial", [
                bootstrap_addr, settler_callback(connect_future)
            ])
            await connect_future
        # Create and start broadcast channel
        self.broadcast_channel = BroadcastChannel(self._node, "dpdl-broadcast")
        await self.broadcast_channel.start()
    async def stop(self):
        # Stop P2P node
        stop_future = Future()
        await self._node.call("stop", [
            settler_callback(stop_future)
        ])
        await stop_future
    async def connect(self, addr):
        # Connect to address
        connect_future = Future()
        await self._node.call("dialProtocol", [
            addr, DPDL_PROTOCOL_NAME, settler_callback(connect_future)
        ])
        js_conn = await connect_future
        # Create peer
        peer = await self._create_peer(js_conn)
        # Generate simulated latency and bandwidth for the connection
        if self._rate_limit_simulator:
            latency, bandwidth = self._rate_limit_simulator.gen_latency_bandwidth()
        else:
            latency = bandwidth = 0
        # Set latency, bandwidth and bandwidth simulator
        connection = peer.connection
        rate_limit_controller = connection._rate_limit_controller
        rate_limit_controller.simulate_latency = latency
        rate_limit_controller.simulate_bandwidth = bandwidth
        rate_limit_controller.rate_limit_simulator = self._rate_limit_simulator
        # Send latency and bandwidth information over the connection
        connection.tx.on_next({
            "msg_type": MSG_NET_BANDWIDTH_INFO,
            "bandwidth": bandwidth,
            "latency": latency
        })
        _logger.debug("Bandwidth information sent to peer")
        # Return peer
        return peer
