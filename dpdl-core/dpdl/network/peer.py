from asyncio import Future

from dpdl.utils.js_bridge import settler_callback
from .connection import LoopbackConnection

class Peer(object):
    def __init__(self, g, connection, peer_info, peer_id=None):
        ## JS bridge global scope
        self._g = g
        ## Connection object
        self.connection = connection
        ## Underlying JS DPDL peer information object
        self.peer_info = peer_info
        ## Cached PeerId object
        self._peer_id = peer_id
        ## Cached ID
        self._id = None
        ## Cached Multiaddr
        self._address = None
    @staticmethod
    async def self_new(g):
        # Create PeerId
        peer_id_future = Future()
        peer_id_class = await g.get("PeerId")
        await peer_id_class.call("create", [
            settler_callback(peer_id_future)
        ])
        peer_id = await peer_id_future
        # Create PeerInfo
        peer_info_class = await g.get("PeerInfo")
        peer_info = await peer_info_class.new([
            peer_id
        ])
        # Create peer object
        return Peer(g, LoopbackConnection(), peer_info, peer_id)
    @staticmethod
    async def self_existing(js_bridge, peer_info_dict):
        g = await js_bridge.g()
        # Peer information dictionary
        peer_info_dict_obj = await js_bridge.obj(peer_info_dict)
        # Create PeerId
        peer_id_future = Future()
        peer_id_class = await g.get("PeerId")
        await peer_id_class.call("createFromJSON", [
            peer_info_dict_obj,
            settler_callback(peer_id_future)
        ])
        peer_id = await peer_id_future
        # Create PeerInfo
        peer_info_class = await g.get("PeerInfo")
        peer_info = await peer_info_class.new([
            peer_id
        ])
        # Create peer object
        return Peer(g, LoopbackConnection(), peer_info, peer_id)
    async def get_peer_id(self):
        """ Return the PeerId object of the peer. """
        # Get and cache PeerId object
        _peer_id = self._peer_id
        if not _peer_id:
            _peer_id = self._peer_id = await self.peer_info.get("id")
        return _peer_id
    async def get_id(self):
        """ Return the Multihash of the peer. """
        # Get and cache ID
        _id = self._id
        if not _id:
            peer_id = await self.get_peer_id()
            _id = self._id = await peer_id.get("id")
        return _id
    async def get_address(self):
        """ Return the Multiaddr of the peer. """
        # Use cached Multiaddr
        if self._address:
            return self._address
        # Get all Multiaddrs of the peer
        multiaddr_set = await self.peer_info.get("multiaddrs")
        multiaddrs = await multiaddr_set.call("toArray")
        # Get and cache the first Multiaddr
        multiaddr_obj = await multiaddrs.get(0)
        multiaddr = self._address = await multiaddr_obj.call("toString")
        return multiaddr
    async def set_address(self, addr):
        """ Set the address of the peer. """
        multiaddr_set = await self.peer_info.get("multiaddrs")
        # Add an Multiaddr
        await multiaddr_set.call("add", [
            addr
        ])
        # Invalidate cache
        self._address = None
    async def to_json(self):
        """ Convert peer information to JSON. """
        peer_id = await self.get_peer_id()
        peer_id_json = await peer_id.call("toJSON")
        # JSON object
        return {
            "id": await peer_id_json.get("id"),
            "privKey": await peer_id_json.get("privKey"),
            "pubKey": await peer_id_json.get("pubKey")
        }
