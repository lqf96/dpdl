import random, asyncio, functools, math
from rx import AnonymousObservable

from dpdl.utils.io import future_ensured

## Send interval
SEND_INTERVAL = 0.1

class RateLimitController(object):
    """ Rate limit controller class. """
    def __init__(self, simulate_latency, simulate_bandwidth, rate_limit_simulator):
        ## Simulated latency
        self.simulate_latency = simulate_latency
        ## Simulated bandwidth
        self.simulate_bandwidth = simulate_bandwidth
        ## Rate limit simulator
        self.rate_limit_simulator = rate_limit_simulator
        ## Sending queue
        self._sending_queue = bytearray()
        ## Event loop
        self._loop = asyncio.get_event_loop()
    def _handle_send_data(self, observer, data):
        simulate_latency = self.simulate_latency
        activate_send_data = not bool(self._sending_queue)
        # Append data to queue
        self._sending_queue += data
        if activate_send_data:
            # No simulated latency
            if simulate_latency==0:
                self._do_send_data(observer)
            # Has simulated latency
            else:
                self._loop.call_later(simulate_latency, self._do_send_data, observer)
    @future_ensured
    async def _do_send_data(self, observer):
        simulate_bandwidth = self.simulate_bandwidth
        # No data to send
        if not self._sending_queue:
            return
        # No simulated bandwidth
        if simulate_bandwidth==0:
            observer.on_next(bytes(self._sending_queue))
            self._sending_queue.clear()
            return
        # Has simulated bandwidth
        simulator = self.rate_limit_simulator
        # Add active connection
        simulator.add_active_conn(self)
        # Keep sending data
        while self._sending_queue:
            # Get connection bandwidth
            conn_bandwidth = simulator.conn_bandwidth(self, SEND_INTERVAL)
            # Update queue
            send_data, self._sending_queue = \
                self._sending_queue[:conn_bandwidth], self._sending_queue[conn_bandwidth:]
            # Send data
            observer.on_next(send_data)
            # Wait for interval
            await asyncio.sleep(SEND_INTERVAL)
        # Remove active connection
        simulator.remove_active_conn(self)
    def throttle(self, parent):
        # Subscribe function
        def subscribe(observer):
            return parent.subscribe(
                on_next=functools.partial(self._handle_send_data, observer),
                on_error=observer.on_error,
                on_completed=observer.on_completed
            )
        return AnonymousObservable(subscribe)

class RateLimitSimulator(object):
    """ Rate limit simulator class. """
    def __init__(self, total_bandwidth, simulation_strategy):
        ## Total upload bandwidth
        self.total_bandwidth = total_bandwidth*128*1000
        ## Simulation strategy
        self.simulation_strategy = simulation_strategy
        ## Active connection pool
        self._active_conn = []
    def gen_latency_bandwidth(self):
        return self.simulation_strategy()
    def add_active_conn(self, controller):
        self._active_conn.append(controller)
    def remove_active_conn(self, controller):
        self._active_conn.remove(controller)
    def conn_bandwidth(self, controller, interval):
        per_conn_bandwidth = self.total_bandwidth/len(self._active_conn)
        bandwidth = min(per_conn_bandwidth, controller.simulate_bandwidth)
        # Total upload bandwidth averaged between connections
        return math.floor(bandwidth*interval)

def fixed_latency_bandwidth(latency, bandwidth):
    # Convert latency and bandwidth
    latency /= 1000
    bandwidth *= 128*1000
    # Strategy
    return lambda: (latency, bandwidth)

def random_latency_bandwidth(l_min=10, l_max=200, b_min=2, b_max=10):
    def strategy():
        # Random latency (deafults to 10ms - 200ms)
        latency = random.randint(l_min, l_max)/1000
        # Random connection bandwidth (defaults to 2Mb - 10Mb)
        bandwidth = random.randint(b_min, b_max)*128*1024
        return latency, bandwidth
    return strategy
