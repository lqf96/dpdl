""" Abstract definitions for training tasks. """
from abc import ABC, abstractproperty

class Task(ABC):
    """ Abstract training task class. """
    def __init__(self, network, params, device):
        """
        Initialize a generic task.
        
        :param params: Parameters for training task.
        """
        ## PyTorch network instance
        self.network = network
        ## Parameters for training task
        self.params = params
        ## Device to use
        self.device = device
    async def train(self, loader, primitives):
        """
        Train the neural network with given data.

        :param loader: PyTorch data loader.
        :param primitives: Primitives for distributed paralleled deep learning.
        :returns: A future resolved when training task is completed.
        """
        raise NotImplementedError()
    async def apply(self, input):
        """
        Apply to the neural network with given data.

        :param input: Input data to apply.
        :returns: A future that resolves to the apply result.
        """
        raise NotImplementedError()
    @abstractproperty
    def backends(self):
        """ Possible backends for this training task. """
        raise NotImplementedError()