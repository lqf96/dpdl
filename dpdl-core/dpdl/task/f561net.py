""" UCI HAR 561-dimension feed-forward neural network example. """
import os, asyncio, time
import numpy as np
import torch

from torch.autograd import Variable
from torch.nn import Module, Linear, CrossEntropyLoss
from torch.optim import SGD
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import TensorDataset
import torch.nn.functional as f

from .abc import Task

class F561Net(Module):
    """ UCI HAR 561-dimension feed-forward neural network. """
    def __init__(self, hidden_size):
        """
        Initialize neural network.

        :param hidden_size: Size of hidden layer.
        """
        super(F561Net, self).__init__()
        # Linear transforms
        self._fc1 = Linear(in_features=561, out_features=hidden_size)
        self._fc2 = Linear(in_features=hidden_size, out_features=6)
    def forward(self, x):
        """
        Describe neural network topology.

        :param x: Input tensor.  
        :returns: Result tensor.
        """
        # Fully-connected layers
        x = f.relu(self._fc1(x))
        # Dropout
        x = f.dropout(x, training=self.training)
        # Output
        return f.log_softmax(self._fc2(x), dim=1)

class F561Task(Task):
    """ F561 FFNN task class. """
    def __init__(self, params, device, **kwargs):
        # Default value for parameters
        params.setdefault("f561_hidden_size", 100)
        params.setdefault("learning_rate", 0.01)
        params.setdefault("step_size", 2000)
        # F561 feed-forward network
        network = F561Net(hidden_size=params["f561_hidden_size"])
        network = network.to(device)
        # Initialize task
        super(F561Task, self).__init__(
            network=network,
            params=params,
            device=device,
            **kwargs
        )
    async def train(self, loader, primitives):
        network = self.network.to(self.device)
        params = self.params
        # Loss and optimizer
        criterion = CrossEntropyLoss()
        optimizer = SGD(network.parameters(), lr=params["learning_rate"])
        # Step learning rate
        scheduler = StepLR(optimizer, step_size=params["step_size"], gamma=0.5)
        # Training epochs
        for global_epoch in range(params["n_global_epochs"]):
            data_iter = iter(loader)
            for local_epoch in range(params["n_local_epochs"]):
                inputs, labels = next(data_iter)
                # Convert tensor to variable
                inputs = inputs.to(self.device)
                labels = labels.to(self.device)
                # Forward
                optimizer.zero_grad()
                outputs = network(inputs)
                # Backward
                loss = criterion(outputs, labels)
                loss.backward()
                # Optimize
                scheduler.step()
                optimizer.step()
                # Temporarily hang up coroutine
                await asyncio.sleep(0)
                # Log loss after every local epoch
                primitives.log_epoch(global_epoch, local_epoch, loss=loss.data.item())
            # "Average" parameters for the neural network and report epoch data
            await asyncio.gather(
                primitives.average(network, global_epoch),
                primitives.flush()
            )
    async def apply(self, inputs):
        with torch.no_grad():
            inputs_device = inputs.device
            inputs = inputs.to(self.device)
            # Calculate output
            outputs = self.network(inputs).data
            # Convert and return output
            return outputs.to(inputs_device)
    ## Possible backends for this training task
    backends = [
        "dpdl.backend.dummy.DummyBackend",
        "dpdl.backend.c_psgd.CPSGDBackend",
        "dpdl.backend.d_psgd.DPSGDBackend"
    ]

def har561_training_dataset(config):
    """ UCI HAR 561 training dataset. """
    base_path = config["base_path"]
    # Load training features and labels
    training_features = torch.from_numpy(np.load(os.path.join(base_path, "train-features.npy")))
    training_labels = torch.from_numpy(np.load(os.path.join(base_path, "train-labels.npy")))
    # Create dataset
    return TensorDataset(training_features, training_labels)

def har561_testing_dataset(config):
    """ UCI HAR 561 testing dataset. """
    base_path = config["base_path"]
    # Load testing features and labels
    testing_features = torch.from_numpy(np.load(os.path.join(base_path, "test-features.npy")))
    testing_labels = torch.from_numpy(np.load(os.path.join(base_path, "test-labels.npy")))
    # Create dataset
    return TensorDataset(testing_features, testing_labels)
