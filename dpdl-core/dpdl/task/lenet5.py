""" LeNet-5 network example. """
import asyncio, tempfile, logging, os
import torch
from torch.autograd import Variable
from torch.nn import Module, Linear, Conv2d, CrossEntropyLoss
from torch.optim import SGD
from torch.optim.lr_scheduler import StepLR
import torch.nn.functional as f
from torchvision.datasets import MNIST
from torchvision import transforms

from .abc import Task

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

class LeNet5(Module):
    """ LeNet-5 neural network. """
    def __init__(self):
        """ Initialize neural network. """
        super(LeNet5, self).__init__()
        # Convolution transforms
        self._conv1 = Conv2d(in_channels=1, out_channels=6, kernel_size=5, padding=2)
        self._conv2 = Conv2d(in_channels=6, out_channels=16, kernel_size=5)
        self._conv3 = Conv2d(in_channels=16, out_channels=120, kernel_size=5)
        # Linear transforms
        self._fc1 = Linear(in_features=120, out_features=84)
        self._fc2 = Linear(in_features=84, out_features=10)
    def forward(self, x):
        """
        Describe neural network topology.

        :param x: Input tensor.  
        :returns: Result tensor.
        """
        # Convolution layers
        x = f.relu(f.max_pool2d(self._conv1(x), kernel_size=2))
        x = f.relu(f.max_pool2d(self._conv2(x), kernel_size=2))
        x = f.relu(self._conv3(x))
        # Reshape
        x = x.view(-1, 120)
        # Fully-connected layers
        x = f.relu(self._fc1(x))
        # Dropout
        x = f.dropout(x, training=self.training)
        # Output
        return f.log_softmax(self._fc2(x), dim=1)

class LeNet5Task(Task):
    """ LeNet-5 training task class. """
    def __init__(self, params, device, **kwargs):
        # Default value for parameters
        params.setdefault("learning_rate", 0.02)
        params.setdefault("step_size", 100)
        # Initialize task
        super(LeNet5Task, self).__init__(
            network=LeNet5().to(device),
            params=params,
            device=device,
            **kwargs
        )
    async def train(self, loader, primitives):
        network = self.network
        params = self.params
        # Loss and optimizer
        criterion = CrossEntropyLoss()
        optimizer = SGD(network.parameters(), lr=params["learning_rate"])
        # Step learning rate
        scheduler = StepLR(optimizer, step_size=params["step_size"], gamma=0.999)
        # Training epochs
        for global_epoch in range(params["n_global_epochs"]):
            data_iter = iter(loader)
            for local_epoch in range(params["n_local_epochs"]):
                inputs, labels = next(data_iter)
                # Convert tensor to variable
                inputs = inputs.to(self.device)
                labels = labels.to(self.device)
                # Forward
                optimizer.zero_grad()
                outputs = network(inputs)
                # Backward
                loss = criterion(outputs, labels)
                loss.backward()
                # Optimize
                scheduler.step()
                optimizer.step()
                # Temporarily hang up coroutine
                await asyncio.sleep(0)
                # Log loss after every local epoch
                primitives.log_epoch(global_epoch, local_epoch, loss=loss.data.item())
            # "Average" parameters for the neural network and report epoch data
            await asyncio.gather(
                primitives.average(network, global_epoch),
                primitives.flush()
            )
    async def apply(self, inputs):
        with torch.no_grad():
            inputs_device = inputs.device
            inputs = inputs.to(self.device)
            # Calculate output
            outputs = self.network(inputs).data
            # Convert and return output
            return outputs.to(inputs_device)
    ## Possible backends for this training task
    backends = [
        "dpdl.backend.dummy.DummyBackend",
        "dpdl.backend.c_psgd.CPSGDBackend",
        "dpdl.backend.d_psgd.DPSGDBackend"
    ]

## MNIST downloading path
mnist_path = None

def mnist_training_dataset(config):
    """ Get MNIST training dataset. """
    return MNIST(
        config["base_path"],
        train=True,
        transform=transforms.ToTensor()
    )

def mnist_testing_dataset(config):
    """ Get MNIST testing dataset. """
    # MNIST testing dataset
    return MNIST(
        config["base_path"],
        train=False,
        transform=transforms.ToTensor()
    )
