""" VGG network example. """
import asyncio

import torch
from torch.nn import Module, Linear, Conv2d, BatchNorm2d, CrossEntropyLoss
from torch.autograd import Variable
from torch.optim import SGD
from torch.optim.lr_scheduler import StepLR
import torch.nn.functional as f

from .abc import Task

class VGGNet(Module):
    """ VGG neural network class. """
    def __init__(self):
        """ Initialize neural network. """
        super(VGGNet, self).__init__()
        # Two convolution layers with pooling
        self._init_two_conv_pool(1, 1, 64)
        self._init_two_conv_pool(2, 64, 128)
        # Three convolution layers with pooling
        self._init_three_conv_pool(3, 128, 256)
        self._init_three_conv_pool(4, 256, 512)
        self._init_three_conv_pool(5, 512, 512)
        # Linear transforms
        self._fc1 = Linear(in_features=512, out_features=64)
        self._fc2 = Linear(in_features=64, out_features=10)
    def _init_two_conv_pool(self, group_id, in_channels, out_channels): 
        conv_tmpl = "_conv_{}_{}"
        batch_norm_tmpl = "_batch_norm_{}_{}"
        # Transforms
        conv1 = Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=3, padding=1)
        conv2 = Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, padding=1)
        norm1 = BatchNorm2d(out_channels)
        norm2 = BatchNorm2d(out_channels)
        # Set transforms
        setattr(self, conv_tmpl.format(group_id, 1), conv1)
        setattr(self, conv_tmpl.format(group_id, 2), conv2)
        setattr(self, batch_norm_tmpl.format(group_id, 1), norm1)
        setattr(self, batch_norm_tmpl.format(group_id, 2), norm2)
    def _init_three_conv_pool(self, group_id, in_channels, out_channels):
        conv_tmpl = "_conv_{}_{}"
        batch_norm_tmpl = "_batch_norm_{}_{}"
        # Transforms
        conv1 = Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=3, padding=1)
        conv2 = Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, padding=1)
        conv3 = Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, padding=1)
        norm1 = BatchNorm2d(out_channels)
        norm2 = BatchNorm2d(out_channels)
        norm3 = BatchNorm2d(out_channels)
        # Set transforms
        setattr(self, conv_tmpl.format(group_id, 1), conv1)
        setattr(self, conv_tmpl.format(group_id, 2), conv2)
        setattr(self, conv_tmpl.format(group_id, 3), conv3)
        setattr(self, batch_norm_tmpl.format(group_id, 1), norm1)
        setattr(self, batch_norm_tmpl.format(group_id, 2), norm2)
        setattr(self, batch_norm_tmpl.format(group_id, 3), norm3)
    def _two_conv_pool(self, group_id, x):
        conv_tmpl = "_conv_{}_{}"
        batch_norm_tmpl = "_batch_norm_{}_{}"
        # Transforms
        conv1 = getattr(self, conv_tmpl.format(group_id, 1))
        conv2 = getattr(self, conv_tmpl.format(group_id, 2))
        norm1 = getattr(self, batch_norm_tmpl.format(group_id, 1))
        norm2 = getattr(self, batch_norm_tmpl.format(group_id, 2))
        # Apply transforms
        x = f.relu(norm1(conv1(x)))
        x = f.relu(norm2(conv2(x)))
        return f.max_pool2d(x, kernel_size=2)
    def _three_conv_pool(self, group_id, x):
        conv_tmpl = "_conv_{}_{}"
        batch_norm_tmpl = "_batch_norm_{}_{}"
        # Transforms
        conv1 = getattr(self, conv_tmpl.format(group_id, 1))
        conv2 = getattr(self, conv_tmpl.format(group_id, 2))
        conv3 = getattr(self, conv_tmpl.format(group_id, 3))
        norm1 = getattr(self, batch_norm_tmpl.format(group_id, 1))
        norm2 = getattr(self, batch_norm_tmpl.format(group_id, 2))
        norm3 = getattr(self, batch_norm_tmpl.format(group_id, 3))
        # Apply transforms
        x = f.relu(norm1(conv1(x)))
        x = f.relu(norm2(conv2(x)))
        x = f.relu(norm3(conv3(x)))
        return f.max_pool2d(x, kernel_size=2)
    def forward(self, x):
        """
        Describe neural network topology.

        :param x: Input tensor.  
        :returns: Result tensor.
        """
        # Pad input
        x = f.pad(x, (2, 2, 2, 2))
        # Two convolution layers with pooling
        x = self._two_conv_pool(1, x)
        x = self._two_conv_pool(2, x)
        # Three convolution layers with pooling
        x = self._three_conv_pool(3, x)
        x = self._three_conv_pool(4, x)
        x = self._three_conv_pool(5, x)
        # Reshape
        x = x.view(-1, 512)
        # Fully-connected layers
        x = f.relu(self._fc1(x))
        # Dropout
        x = f.dropout(x, training=self.training)
        # Output
        return f.log_softmax(self._fc2(x), dim=1)

class VGGTask(Task):
    """ VGG network training task class. """
    def __init__(self, params, device, **kwargs):
        # Default value for parameters
        params.setdefault("learning_rate", 0.001)
        params.setdefault("step_size", 100)
        # Initialize task
        super(VGGTask, self).__init__(
            network=VGGNet().to(device),
            params=params,
            device=device,
            **kwargs
        )
    async def train(self, loader, primitives):
        network = self.network
        params = self.params
        # Loss and optimizer
        criterion = CrossEntropyLoss()
        optimizer = SGD(network.parameters(), lr=params["learning_rate"])
        # Step learning rate
        scheduler = StepLR(optimizer, step_size=params["step_size"], gamma=0.999)
        # Training epochs
        for global_epoch in range(params["n_global_epochs"]):
            data_iter = iter(loader)
            for local_epoch in range(params["n_local_epochs"]):
                inputs, labels = next(data_iter)
                # Convert tensor to variable
                inputs = inputs.to(self.device)
                labels = labels.to(self.device)
                # Forward
                optimizer.zero_grad()
                outputs = network(inputs)
                # Backward
                loss = criterion(outputs, labels)
                loss.backward()
                # Optimize
                scheduler.step()
                optimizer.step()
                # Temporarily hang up coroutine
                await asyncio.sleep(0)
                # Log loss after every local epoch
                primitives.log_epoch(global_epoch, local_epoch, loss=loss.data.item())
            # "Average" parameters for the neural network and report epoch data
            await asyncio.gather(
                primitives.average(network, global_epoch),
                primitives.flush()
            )
    async def apply(self, inputs):
        with torch.no_grad():
            inputs_device = inputs.device
            inputs = inputs.to(self.device)
            # Calculate output
            outputs = self.network(inputs).data
            # Convert and return output
            return outputs.to(inputs_device)
    ## Possible backends for this training task
    backends = [
        "dpdl.backend.dummy.DummyBackend",
        "dpdl.backend.c_psgd.CPSGDBackend",
        "dpdl.backend.d_psgd.DPSGDBackend"
    ]
    
