import asyncio, functools
from asyncio import Future
import msgpack, numpy as np, torch
from rx import Observer

## Serialize arguments
SERIALIZE_ARGS = {"use_bin_type": True}
## Deserialize arguments
DESERIALIZE_ARGS = {"raw": False}

def _custom_types_encoder(serializer_mapping):
    def encoder(obj):
        # Find serializer and extension code by type
        obj_type = type(obj)
        serializer_pair = serializer_mapping.get(obj_type)
        # Unknown type
        if not serializer_pair:
            raise TypeError("Unknown type for serialization: {}".format(obj_type))
        # Extension code and serializer
        ext_code, serializer = serializer_pair
        # Serialize inner data and wrap as extension
        inner_data = msgpack.packb(serializer(obj), **SERIALIZE_ARGS)
        return msgpack.ExtType(ext_code, inner_data)
    return encoder

def _custom_types_decoder(deserializer_mapping):
    def decoder(code, data):
        # Find deserializer by extension code
        deserializer = deserializer_mapping.get(code)
        # Fall back to built-in extension type if deserializer not found
        if not deserializer:
            return msgpack.ExtType(code, data)
        # Unpack inner data and deserialize
        inner_data = msgpack.unpackb(data, **DESERIALIZE_ARGS)
        return deserializer(inner_data)
    return decoder

def pack_data(obj, serializer_mapping={}):
    """
    Pack an object using MessagePack.

    :param obj: Object to pack.
    :param custom_type_serializers: Custom type serializers.
    """
    # Encoder
    encoder = _custom_types_encoder(serializer_mapping)
    # Pack object
    return msgpack.packb(obj, default=encoder, **SERIALIZE_ARGS)

def unpack_data(buf, deserializer_mapping={}):
    """
    Unpack a buffer using MessagePack.

    :param buf: Buffer to unpack.
    :param deserializer_mapping: Custom type deserializers.
    """
    # Decoder
    decoder = _custom_types_decoder(deserializer_mapping)
    # Unpack buffer
    return msgpack.unpackb(buf, ext_hook=decoder, **DESERIALIZE_ARGS)

def subset_of(a, b):
    """
    Check whether a dictionary is a subset of another dictionary.
    """
    for key, value in a.items():
        # Key not present or value doesn't match
        if key not in b or b[key]!=value:
            return False
    return True

def of_msg_type(msg_types):
    """
    Return a filter based on the type of the message.

    :param msg_types: Message types for filtering.
    """
    # Single message
    if isinstance(msg_types, str):
        msg_types = set([msg_types])
    # Predicate
    return lambda msg: msg["msg_type"] in msg_types

def filter_recv(rx, predicate):
    """
    Filter next message that matches given data.

    :param rx: Rx stream.
    :param data: Part of the message data to match.
    :returns: A future resolved with next data that matches given data.
    """
    # Convert message type to predicate
    if isinstance(predicate, (str, list)):
        _predicate = of_msg_type(predicate)
    # Convert part of the data to predicate
    elif isinstance(predicate, dict):
        _predicate = lambda msg: subset_of(predicate, msg)
    # Fetch next data that matches given data
    return rx.filter(_predicate).take(1).to_future()

def future_ensured(func):
    """
    A decorator for automatically ensure the execution of a coroutine function.

    :param func: Coroutine function to decorate.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return asyncio.ensure_future(func(*args, **kwargs))
    return wrapper

def as_torch_tensor(params, device):
    # Single array
    if isinstance(params, (np.ndarray, list)):
        return torch.tensor(params, device=device)
    # Parameter dictionary
    elif isinstance(params, dict):
        return {key: as_torch_tensor(param, device) for key, param in params.items()}
    # Unknown type
    else:
        raise TypeError("Unknown type for conversion")

def as_numpy_array(params):
    # Parameter dictionary
    if isinstance(params, dict):
        return {key: as_numpy_array(param) for key, param in params.items()}
    # PyTorch types
    else:
        return params.to("cpu").numpy()
