import struct, socket, sys, asyncio, os
from asyncio import subprocess, Future
from weakref import WeakKeyDictionary, WeakValueDictionary
from msgpack import ExtType

from .io import pack_data, unpack_data

import weakref

## Create new object
JSB_NEW_OBJ = "new_obj"
## Remove object
JSB_REMOVE_OBJ = "remove_obj"
## Get property
JSB_GET_PROP = "get_prop"
## Set property
JSB_SET_PROP = "set_prop"
## Call function
JSB_CALL_FUNC = "call_func"
## Call member method on object
JSB_CALL_MEMBER = "call_member"
## Get global scope
JSB_GLOBAL_SCOPE = "global_scope"

## Default basic types
DEFAULT_BASIC_TYPES = [
    int,
    float,
    str,
    bytes,
    bytearray,
    bool,
    type(None)
]

class JSObject(object):
    """ Remote JS object proxy type. """
    def __init__(self, js_bridge, js_obj_id):
        """
        Initialize a JS object proxy.

        :param js_bridge: JS bridge object.
        :param js_obj_id: JS object ID.
        """
        ## JS bridge object
        self._js_bridge = js_bridge
        ## JS object ID
        self._js_obj_id = js_obj_id
    def __del__(self):
        """ Remove object from JS object table when RC reaches 0. """
        js_bridge = self._js_bridge
        # Exit if bridge is not running
        if not js_bridge._running:
            return
        # Request deletion of JS object
        asyncio.ensure_future(js_bridge._request(JSB_REMOVE_OBJ, {
            "obj": js_bridge._as_handle(self)
        }))
    async def get(self, key):
        """
        Get property of JS object by key.

        :param key: property key.
        :returns: A future that resolves to the 
        """
        js_bridge = self._js_bridge
        # Request property
        resp = await js_bridge._request(JSB_GET_PROP, {
            "obj": js_bridge._as_handle(self),
            "key": js_bridge._as_handle(key)
        })
        # Result
        return js_bridge._unwrap_handle(resp["value"])
    async def set(self, key, value):
        """
        Set property of JS object by key.

        :param key: Property key.
        :param value: Property value.
        :returns: Property value or another JS object proxy.
        """
        js_bridge = self._js_bridge
        # Set property
        await js_bridge._request(JSB_SET_PROP, {
            "obj": js_bridge._as_handle(self),
            "key": js_bridge._as_handle(key),
            "value": js_bridge._as_handle(value)
        })
    async def __call__(self, args=[]):
        """
        Call a callable JS object.

        :param args: Arguments to be passed to the function.
        """
        js_bridge = self._js_bridge
        # Arguments
        func_args = [js_bridge._as_handle(arg) for arg in args]
        # Call function
        resp = await js_bridge._request(JSB_CALL_FUNC, {
            "obj": js_bridge._as_handle(self),
            "args": func_args
        })
        # Raise exception or return result
        if "exception" in resp:
            js_error = js_bridge._unwrap_handle(resp["exception"])
            message = resp["message"]
            stack = resp["stack"]
            # Wrap in JSException
            raise JSException(js_error, message, stack)
        else:
            return js_bridge._unwrap_handle(resp["result"])
    async def call(self, member_key, args=[]):
        """
        Call member function on the JS object.

        :param args: Arguments to be passed to the member function.
        """
        js_bridge = self._js_bridge
        # Arguments
        func_args = [js_bridge._as_handle(arg) for arg in args]
        # Request member function call
        resp = await js_bridge._request(JSB_CALL_MEMBER, {
            "obj": js_bridge._as_handle(self),
            "member_key": js_bridge._as_handle(member_key),
            "args": func_args
        })
        # Raise exception or return result
        if "exception" in resp:
            js_error = js_bridge._unwrap_handle(resp["exception"])
            message = resp["message"]
            stack = resp["stack"]
            # Wrap in JSException
            raise JSException(js_error, message, stack)
        else:
            return js_bridge._unwrap_handle(resp["result"])
    async def new(self, args=[]):
        """
        Create a new JS object using an instance factory.

        :param args: Arguments to be passed to the constructor.
        """
        js_bridge = self._js_bridge
        # Arguments
        func_args = [js_bridge._as_handle(arg) for arg in args]
        # Request new object creation
        resp = await js_bridge._request(JSB_NEW_OBJ, {
            "obj": js_bridge._as_handle(self),
            "args": func_args
        })
        # Raise exception or return result
        if "exception" in resp:
            js_error = js_bridge._unwrap_handle(resp["exception"])
            message = resp["message"]
            stack = resp["stack"]
            # Wrap in JSException
            raise JSException(js_error, message, stack)
        else:
            return js_bridge._unwrap_handle(resp["result"])

class JSHandle(object):
    """ JS handle class. """
    def __init__(self, js_obj_id):
        ## JS object ID
        self.js_obj_id = js_obj_id
    def serialize(self):
        return {"js_obj_id": self.js_obj_id}
    @staticmethod
    def deserialize(obj):
        return JSHandle(obj["js_obj_id"])

class PyHandle(object):
    """ Python handle class. """
    def __init__(self, py_obj_id):
        ## Python object ID
        self.py_obj_id = py_obj_id
    def serialize(self):
        return {"py_obj_id": self.py_obj_id}
    @staticmethod
    def deserialize(obj):
        return PyHandle(obj["py_obj_id"])

## JS bridge serializer mapping
JSB_SERIALIZER_MAPPING = {
    JSHandle: (0x30, JSHandle.serialize),
    PyHandle: (0x31, PyHandle.serialize)
}
## JS bridge deserializer mapping
JSB_DESERIALIZER_MAPPING = {
    0x30: JSHandle.deserialize,
    0x31: PyHandle.deserialize
}

# Maximum I/O size
MAX_IO_SIZE = 131072

class JSBridge(object):
    """ JS bridge class. """
    def __init__(self, reader, writer, basic_types=[]):
        ## Reader
        self.reader = reader
        ## Writer
        self.writer = writer
        ## Basic types
        self.basic_types = set(DEFAULT_BASIC_TYPES+basic_types)
        ## Running flag
        self._running = False
        ## JS object mapping
        self._js_obj_map = WeakValueDictionary()
        ## JS object reverse mapping
        self._js_obj_rmap = WeakKeyDictionary()
        ## Python object mapping
        self._py_obj_map = {}
        ## Python object reverse mapping
        self._py_obj_rmap = {}
        ## Python object count
        self._py_obj_count = 0
        ## Request table
        self._req_table = {}
        ## Request ID count
        self._req_id_count = 0
        ## Write task queue
        self._write_tasks = []
        ## Cached global scope
        self._global_scope = None
        ## Cached Array type
        self._array_type = None
        ## Cached Object type
        self._object_type = None
    def _as_handle(self, obj):
        # Basic types
        if type(obj) in self.basic_types:
            return obj
        # JS object proxy
        if isinstance(obj, JSObject):
            return JSHandle(obj._js_obj_id)
        # Other Python objects
        py_obj_id = self._py_obj_rmap.get(obj)
        if py_obj_id==None:
            py_obj_id = self._py_obj_count
            self._py_obj_count += 1
            # Add object to JS object mappings
            self._py_obj_map[py_obj_id] = obj
            self._py_obj_rmap[obj] = py_obj_id
        # Wrap as Python handle
        return PyHandle(py_obj_id)
    def _unwrap_handle(self, handle):
        # Unwrap Python handle
        if isinstance(handle, PyHandle):
            return self._py_obj_map[handle.py_obj_id]
        # Unwrap JS handle
        if isinstance(handle, JSHandle):
            js_obj_map = self._js_obj_map
            # JS object ID
            js_obj_id = handle.js_obj_id
            # Existing JS object
            if js_obj_id in js_obj_map:
                return js_obj_map[js_obj_id]
            # Create JS object proxy
            obj = JSObject(self, js_obj_id)
            # Add JS object to object mappings
            js_obj_map[js_obj_id] = obj
            self._js_obj_rmap[obj] = js_obj_id
            # Return strong reference to JS object
            return obj
        # Basic types
        return handle
    async def _write_data(self, raw_data):
        # Not running
        if not self._running:
            return
        # Invoke do write function
        if not self._write_tasks:
            asyncio.ensure_future(self._do_write())
        # Add write task
        future = Future()
        self._write_tasks.append((raw_data, future))
        # Return future
        return await future
    async def _do_write(self):
        try:
            writer = self.writer
            while self._write_tasks:
                # Fetch next write task
                data, future = self._write_tasks[0]
                data_len = len(data)
                # Write data size
                writer.write(struct.pack("<L", data_len))
                # Write loop
                offset = 0
                while offset<data_len:
                    write_len = min(data_len-offset, MAX_IO_SIZE)
                    writer.write(data[offset:offset+write_len])
                    # Drain written data
                    await writer.drain()
                    # Update offset
                    offset += write_len
                # Pop write task
                self._write_tasks.pop(0)
                # Resolve future
                future.set_result(None)
        except BrokenPipeError:
            self._running = False
    async def _request(self, msg_type, msg={}):
        # Request ID
        req_id = self._req_id_count
        self._req_id_count += 1
        # Add message type and request ID
        msg["msg_type"] = msg_type
        msg["py_req_id"] = req_id
        # Create a new future and add it to the request table
        f = self._req_table[req_id] = Future()
        # Serialize and send message
        raw_data = pack_data(msg, JSB_SERIALIZER_MAPPING)
        await self._write_data(raw_data)
        # Wait for and return response
        return await f
    async def _handle_js_request(self, msg):
        # Handle message with corresponding handler
        handler = getattr(self, "_py_"+msg["msg_type"])
        resp = handler(msg)
        # Add message type and request ID
        resp["js_req_id"] = msg["js_req_id"]
        resp["msg_type"] = msg["msg_type"]
        # Serialize and send response
        raw_data = pack_data(resp, JSB_SERIALIZER_MAPPING)
        await self._write_data(raw_data)
    def _py_remove_obj(self, args):
        """ Remove object from Python object table. """
        # Get object and ID
        py_obj_id = args["obj"].py_obj_id
        obj = self._unwrap_handle(args["obj"])
        # Remove object from two mappings
        del self._py_obj_map[py_obj_id]
        del self._py_obj_rmap[obj]
        # Response
        return {}
    def _py_call_func(self, args):
        """ Call function with arguments. """
        obj = self._unwrap_handle(args["obj"])
        # Arguments
        func_args = [self._unwrap_handle(arg) for arg in args["args"]]
        # Call function
        try:
            result = obj(*func_args)
        except Exception as e:
            raise e
        result = self._as_handle(result)
        # Response
        return {"result": result}
    async def _js_bridge_loop(self):
        """ Start running JS bridge. """
        try:
            # Read loop
            while self._running:
                # Message length
                msg_len_buf = await self.reader.read(4)
                msg_len, = struct.unpack("<L", msg_len_buf)
                # Keep reading message until whole message is read
                msg_data = bytearray()
                remaining = msg_len
                while remaining>0:
                    recv_data = await self.reader.read(remaining)
                    remaining -= len(recv_data)
                    msg_data += recv_data
                # Deserialize message data
                msg = unpack_data(msg_data, JSB_DESERIALIZER_MAPPING)
                # Handle JS request
                if "js_req_id" in msg:
                    await self._handle_js_request(msg)
                # Handle Python response
                else:
                    py_req_id = msg["py_req_id"]
                    self._req_table[py_req_id].set_result(msg)
        # Subprocess killed
        except BrokenPipeError:
            self._running = False
    def start(self):
        self._running = True
        # Start JS bridge loop
        asyncio.ensure_future(self._js_bridge_loop())
    def stop(self):
        # Set running flag to false
        self._running = False
    async def g(self):
        """ Get global scope of the execution environment. """
        # Use cached global scope
        if self._global_scope:
            return self._global_scope
        # Request and get global scope
        resp = await self._request(JSB_GLOBAL_SCOPE)
        self._global_scope = self._unwrap_handle(resp["value"])
        # Result
        return self._global_scope
    async def array(self, elements=[]):
        """ Create a JS array from an iterable. """
        # Use cached Array type
        array_type = self._array_type
        if not array_type:
            g = await self.g()
            array_type = self._array_type = await g.get("Array")
        # Create JS array
        return await array_type.new(list(elements))
    async def obj(self, member_dict={}):
        """ Create a JS object from a member dictionary. """
        # Use cached Object type
        object_type = self._object_type
        if not object_type:
            g = await self.g()
            object_type = self._object_type = await g.get("Object")
        # Create JS object
        new_obj = await object_type.new()
        for key, value in member_dict.items():
            await new_obj.set(key, value)
        return new_obj

class JSException(Exception):
    """ JS exception wrapper class. """
    def __init__(self, js_error, msg=None, js_stack=None):
        super(JSException, self).__init__(msg)
        ## JS error object
        self.js_error = js_error
        ## JS stack
        self.js_stack = js_stack

async def js_bridge_helper(process_args):
    # Socket pair
    sock_1, sock_2 = socket.socketpair(socket.AF_UNIX)
    # Append arguments
    process_args += ["-s", str(sock_2.fileno())]
    # Sub-process standard output and standard error stream
    stdout_path = os.environ.get("JSB_OUT")
    stderr_path = os.environ.get("JSB_ERR")
    proc_stdout = open(stdout_path, "w") if stdout_path else subprocess.PIPE
    proc_stderr = open(stderr_path, "w") if stderr_path else subprocess.PIPE
    # Create sub-process
    process = await subprocess.create_subprocess_exec(
        *process_args,
        stdout=proc_stdout,
        stderr=proc_stderr,
        pass_fds=[
            sock_2.fileno()
        ]
    )
    # Close unnecessary file descriptors
    sock_2.close()
    if stdout_path:
        proc_stdout.close()
    if stderr_path:
        proc_stderr.close()
    # Relay process message
    async def relay_proc_message(source, destination):
        try:
            while True:
                # Read data from stream
                data = await source.read()
                # Write data to des
                destination.write(data.decode("utf-8"))
        except BrokenPipeError:
            pass
    if not stdout_path:
        asyncio.ensure_future(relay_proc_message(process.stdout, sys.stdout))
    if not stderr_path:
        asyncio.ensure_future(relay_proc_message(process.stderr, sys.stderr))
    # Create reader and writer
    reader, writer = await asyncio.open_connection(sock=sock_1)
    # Create JS bridge
    js_bridge = JSBridge(reader, writer)
    # Return JS bridge and process
    return js_bridge, process

def settler_callback(future):
    """
    Make a settler callback for an AsyncIO future.

    :param future: AsyncIO future object.
    :returns: A settler callback function.
    """
    def _callback(error=None, result=None):
        # Rejected
        if error!=None:
            future.set_exception(JSException(error))
        # Fulfilled
        else:
            future.set_result(result)
    return _callback

def as_py_future(js_promise):
    """
    Convert JS promise object into AsyncIO future.

    :param js_promise: JS promise object.
    :returns: AsyncIO future object.
    """
    f = Future()
    # Fulfilled callback
    def _on_fulfilled(result):
        f.set_result(result)
    # Rejected callback
    def _on_rejected(error):
        f.set_exception(JSException(error))
    # Register callback
    js_promise.call("then", [
        _on_fulfilled,
        _on_rejected
    ])
    return f
