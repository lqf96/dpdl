from importlib import import_module

def split_class_path(class_path):
    """
    Split class path into module path and class name.

    :param class_path: Class path.
    :returns: Module path and class name.
    """
    # Find index of last dot
    for i in range(len(class_path)):
        dot_index = -1*i-1
        if class_path[dot_index]==".":
            break
    # Module path and class name
    return class_path[:dot_index], class_path[dot_index+1:]

def as_class(class_path):
    """
    Convert class path to class.

    :param class_path: Class path.
    """
    # Ignore classes
    if isinstance(class_path, type):
        return class_path
    # Module path and class name
    module_path, class_name = split_class_path(class_path)
    # Import package
    module = import_module(module_path)
    # Get class
    return getattr(module, class_name)

def as_class_path(_class):
    """
    Convert class to class path.

    :param _class: Class.
    """
    # Ignore class path
    if isinstance(_class, str):
        return _class
    return _class.__module__+"."+_class.__name__