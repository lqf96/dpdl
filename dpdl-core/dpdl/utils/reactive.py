import struct
from asyncio import Future
from rx import AnonymousObservable, Observer

from dpdl.utils.io import pack_data, unpack_data, future_ensured

# JS exception reference (lazily loaded to avoid errors)
_JSException = None

def _js_exp_class():
    global _JSException
    # Lazily load JSException
    if not _JSException:
        from .js_bridge import JSException
        _JSException = JSException
    return _JSException

def _as_pull_stream_error(error):
    # Error must be a base class of BaseException
    if not isinstance(error, BaseException):
        raise TypeError("Error must be a subclass of BaseException")
    # JS exception
    if isinstance(error, _js_exp_class()):
        return error.js_error
    # Stop iteration
    elif isinstance(error, StopIteration):
        return True
    # Other Python exception
    else:
        return error

def msg_encoder(parent, serializer_mapping):
    # Subscribe function
    def subscribe(observer):
        # Data handler
        def on_next(msg):
            # Serialize message
            msg_data = pack_data(msg, serializer_mapping)
            # Prepend length of message
            msg_len = struct.pack("<L", len(msg_data))
            msg_data = msg_len+msg_data
            # Inform observer
            observer.on_next(msg_data)
        # Add subscription
        return parent.subscribe(
            on_next=on_next,
            on_error=observer.on_error,
            on_completed=observer.on_completed
        )
    return AnonymousObservable(subscribe)

def msg_decoder(parent, deserializer_mapping):
    # Subscribe function
    def subscribe(observer):
        # Received message length
        recv_msg_len = None
        # Received message buffer
        recv_msg_buf = bytearray()
        # Data handler
        def on_next(data):
            nonlocal recv_msg_len
            nonlocal recv_msg_buf
            # Update message buffer
            recv_msg_buf += data
            while True:
                # Get message length
                if recv_msg_len==None:
                    if len(recv_msg_buf)>=4:
                        recv_msg_len, = struct.unpack("<L", recv_msg_buf[:4])
                        recv_msg_buf = recv_msg_buf[4:]
                    else:
                        break
                # Get message
                if len(recv_msg_buf)>=recv_msg_len:
                    msg_data = recv_msg_buf[:recv_msg_len]
                    # Deserialize message
                    msg = unpack_data(msg_data, deserializer_mapping)
                    # Clear buffer
                    recv_msg_buf = recv_msg_buf[recv_msg_len:]
                    recv_msg_len = None
                    # Inform observer
                    observer.on_next(msg)
                else:
                    break
        # Add subscription
        return parent.subscribe(
            on_next=on_next,
            on_error=observer.on_error,
            on_completed=observer.on_completed
        )
    return AnonymousObservable(subscribe)

class PullStreamSource(Observer):
    def __init__(self):
        ## Data queue
        self._data_queue = []
        ## Callback queue
        self._cb_queue = []
    @future_ensured
    async def source(self, end, cb):
        # Aborted
        if end:
            return await cb([end])
        # Resolve with data if available
        if self._data_queue:
            next_item = self._data_queue.pop(0)
            # Exception
            if isinstance(next_item, BaseException):
                error = _as_pull_stream_error(next_item)
                return await cb([error])
            # Data
            else:
                return await cb([None, next_item])
        # Save callback in queue
        else:
            self._cb_queue.append(cb)
    @future_ensured
    async def on_next(self, value):
        # Resolve with data if callback is available
        if self._cb_queue:
            cb = self._cb_queue.pop(0)
            await cb([None, value])
        # Save data in queue
        else:
            self._data_queue.append(value)
    @future_ensured
    async def on_error(self, error):
        # Resolve with error if callback is available
        if self._cb_queue:
            cb = self._cb_queue.pop(0)
            error = _as_pull_stream_error(error)
            await cb([error])
        # Save error in queue
        else:
            self._data_queue.append(error)
    def on_completed(self):
        # Treat as a StopIteration exception
        self.on_error(StopIteration())

class PullStreamSink(AnonymousObservable):
    def __init__(self):
        # Subscribe function
        def subscribe(observer):
            # Add new observer to list
            self._observers.append(observer)
        # Initalize base class
        super(PullStreamSink, self).__init__(subscribe)
        ## Observers list
        observers = self._observers = []
    @future_ensured
    async def sink(self, end, data):
        # Completed
        if end==True:
            for observer in self._observers:
                observer.on_completed()
            self._observers.clear()
            return
        # Error occured
        elif end!=None:
            error = _js_exp_class()(end)
            for observer in self._observers:
                observer.on_error(error)
        # Data
        else:
            for observer in self._observers:
                observer.on_next(data)
