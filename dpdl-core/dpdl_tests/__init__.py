from unittest import makeSuite, TestSuite, TextTestRunner

from .js_bridge import JSBridgeTest

# Make test suite
test_suite = TestSuite()
# Add test units
test_suite.addTest(makeSuite(JSBridgeTest))
