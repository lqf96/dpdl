import asyncio, sys
from asyncio import Future
from unittest import TestCase
from dpdl.utils.js_bridge import js_bridge_helper, JSBridge, JSException
from dpdl.utils.io import future_ensured

from .utils import async_wrapper

class JSBridgeTest(TestCase):
    @classmethod
    def setUpClass(cls):
        # Set default event loop
        loop = cls._loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        # Initialize test
        cls._init_test()
    @classmethod
    def tearDownClass(cls):
        # Clear objects
        cls._g = None
        cls._test_func_1 = None
        cls._test_obj = None
        # Wait for 0.1 second and close the loop
        loop = cls._loop
        loop.run_until_complete(asyncio.sleep(0.1))
        loop.close()
        # Kill sub-process
        cls._process.kill()
    @classmethod
    @async_wrapper
    async def _init_test(cls):
        ## JS bridge and sub-process
        cls._js_bridge, cls._process = await js_bridge_helper(["js-bridge-test"])
        # Start JS bridge
        cls._js_bridge.start()
    @classmethod
    @async_wrapper
    async def test_00_get_global_scope(cls):
        # Get global scope
        g = cls._g = await cls._js_bridge.g()
    @async_wrapper
    async def test_01_get_prop(self):
        # Get test property
        self.assertEqual(await self._g.get("test_prop_1"), 42)
    @async_wrapper
    async def test_02_set_prop(self):
        g = self._g
        # Set test property
        await g.set("test_prop_2", "42")
        # Get test property
        self.assertEqual(await g.get("test_prop_2"), "42")
    @async_wrapper
    async def test_03_null_value(self):
        g = self._g
        # Set test property
        await g.set("test_prop_3", None)
        # Get test property
        self.assertEqual(await g.get("test_prop_3"), None)
    @async_wrapper
    async def test_04_call_func(self):
        # Call test function
        test_func_1 = await self._g.get("test_func_1")
        # Result
        self.assertEqual(await test_func_1([42]), 84)
    @async_wrapper
    async def test_05_call_func_exception(self):
        # Call test function
        with self.assertRaises(JSException) as ctx:
            test_func_2 = await self._g.get("test_func_2")
            await test_func_2()
            # Exception message
            self.assertEqual(ctx.exception.args[0], "Error: Test error")
    @classmethod
    @async_wrapper
    async def test_06_new_obj(cls):
        g = cls._g
        # Parameters
        cls._test_func_1 = test_func_1 = await g.get("test_func_1")
        # Create new test object
        js_bridge_test = await g.get("JSBridgeTest")
        cls._test_obj = await js_bridge_test.new([True, test_func_1])
    @async_wrapper
    async def test_07_call_member_method(self):
        # Call member method
        result = await self._test_obj.call("test_member_method", ["123"])
        # Result
        self.assertEqual(result, b"true 123")
    @async_wrapper
    async def test_08_create_obj(self):
        # Create object
        test_obj = await self._js_bridge.obj({
            "a": b"abc",
            "b": self._test_func_1
        })
        # Asserts members
        self.assertEqual(await test_obj.get("a"), b"abc")
        self.assertIs(await test_obj.get("b"), self._test_func_1)
    @async_wrapper
    async def test_09_create_array(self):
        # Create array
        test_array = await self._js_bridge.array([b"abc", self._test_func_1])
        # Assert elements
        self.assertEqual(await test_array.get(0), b"abc")
        self.assertIs(await test_array.get(1), self._test_func_1)
    @async_wrapper
    async def test_10_js_py_obj_interop(self):
        f = Future()
        # Python callback function
        @future_ensured
        async def py_callback(test_func):
            print("Callback is invoked!")
            self.assertIs(test_func, self._test_func_1)
            # Call function again
            self.assertEqual(await test_func([42]), 84)
            # Resolve future
            f.set_result(None)
        # Call member method
        await self._test_obj.call("test_invoke_py_callback", [
            py_callback
        ])
        return f
