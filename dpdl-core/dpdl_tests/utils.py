import functools

def async_wrapper(member_func):
    @functools.wraps(member_func)
    def wrapper(self, *args, **kwargs):
        self._loop.run_until_complete(member_func(self, *args, **kwargs))
    return wrapper
