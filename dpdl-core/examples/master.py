#! /usr/bin/env python3
import asyncio, os, json, logging, sys
from argparse import ArgumentParser
import torch
from torch.utils.data import DataLoader

from dpdl.engine import Engine
from dpdl.network import Network, Peer
from dpdl.network.rate_limit import RateLimitSimulator, fixed_latency_bandwidth
from dpdl.utils.loader import as_class
from dpdl.utils.js_bridge import js_bridge_helper

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

## Module directory path
module_dir = os.path.dirname(__file__)

## Backend configurations
BACKEND_CONFIG = {
    # Dummy
    "Dummy": {
        "backend_class": "dpdl.backend.dummy.DummyBackend",
        "backend_config": {}
    },
    # C-PSGD
    "CPSGD": {
        "backend_class": "dpdl.backend.c_psgd.CPSGDBackend",
        "backend_config": {
            "share_init_params": True
        }
    },
    # D-PSGD
    "DPSGD": {
        "backend_class": "dpdl.backend.d_psgd.DPSGDBackend",
        "backend_config": {
            "share_init_params": True,
            "n_shortcut_neighbors": 0
        }
    }
}

## Training task configurations
TRAINING_TASK_CONFIG = {
    # UCI HAR 561
    "F561": {
        "task_class": "dpdl.task.f561net.F561Task",
        "task_config": {
            "n_global_epochs": 500,
            "n_local_epochs": 20
        },
        "dataset_class": "dpdl.task.f561net.har561_training_dataset",
        "dataset_config": {
            "base_path": os.path.join(module_dir, "../../dataset/har561"),
            "batch_size": 64
        }
    },
    # LeNet-5
    "LeNet5": {
        "task_class": "dpdl.task.lenet5.LeNet5Task",
        "task_config": {
            "n_global_epochs": 500,
            "n_local_epochs": 20
        },
        "dataset_class": "dpdl.task.lenet5.mnist_training_dataset",
        "dataset_config": {
            "base_path": os.path.join(module_dir, "../../dataset/mnist"),
            "batch_size": 64
        }
    },
    # VGGNet
    "VGGNet": {
        "task_class": "dpdl.task.vggnet.VGGTask",
        "task_config": {
            "n_global_epochs": 500,
            "n_local_epochs": 20
        },
        "dataset_class": "dpdl.task.lenet5.mnist_training_dataset",
        "dataset_config": {
            "base_path": os.path.join(module_dir, "../../dataset/mnist"),
            "batch_size": 64
        }
    }
}

## Testing dataset configurations
TESTING_DATASET_CONFIG = {
    # UCI HAR 561
    "F561": {
        "dataset_class": "dpdl.task.f561net.har561_testing_dataset",
        "dataset_config": {
            "base_path": os.path.join(module_dir, "../../dataset/har561")
        }
    },
    # LeNet-5
    "LeNet5": {
        "dataset_class": "dpdl.task.lenet5.mnist_testing_dataset",
        "dataset_config": {
            "base_path": os.path.join(module_dir, "../../dataset/mnist")
        }
    },
    # VGGNet
    "VGGNet": {
        "dataset_class": "dpdl.task.lenet5.mnist_training_dataset",
        "dataset_config": {
            "base_path": os.path.join(module_dir, "../../dataset/mnist")
        }
    }
}

def main():
    parser = ArgumentParser()
    # Arguments
    parser.add_argument(
        "-p", "--peer-info",
        help="JSON file for loading or saving peer information.",
        required=True
    )
    parser.add_argument(
        "-a", "--multiaddr",
        help="Multiaddr to listen on.",
        default="/ip4/127.0.0.1/tcp/0"
    )
    parser.add_argument(
        "--bootstrap-addr",
        help="P2P bootstrapping address."
    )
    parser.add_argument(
        "-n", "--network",
        help="Neural network for training.",
        choices=TRAINING_TASK_CONFIG.keys(),
        required=True
    )
    parser.add_argument(
        "-b", "--backend",
        help="Training backend to use.",
        choices=BACKEND_CONFIG.keys(),
        default="Dummy"
    )
    parser.add_argument(
        "-d", "--device",
        help="Device for training.",
        choices=["cuda", "cpu"],
        default="cpu"
    )
    parser.add_argument(
        "-c", "--n-workers",
        help="Number of workers participated in training.",
        type=int,
        default=1
    )
    parser.add_argument(
        "-t", "--training-name",
        help="Name of this training."
    )
    parser.add_argument(
        "-s", "--training-stats",
        help="File to save training statistics."
    )
    # Parse arguments
    args = parser.parse_args()
    # Set default event loop
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    # Setup logger
    logging.basicConfig(
        format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s",
        stream=sys.stderr
    )
    # Start function
    loop.run_until_complete(start(args))
    return 0

async def start(args):
    try:
        # Create JS bridge
        js_bridge, js_process = await js_bridge_helper(["dpdl-io"])
        js_bridge.start()
        _logger.debug("JS bridge started")
        # Load or create peer information file
        g = await js_bridge.g()
        if os.path.exists(args.peer_info):
            with open(args.peer_info) as f:
                peer_info_dict = json.load(f)
                self_peer = await Peer.self_existing(js_bridge, peer_info_dict)
                _logger.debug("Peer information loaded")
        else:
            with open(args.peer_info, "w") as f:
                self_peer = await Peer.self_new(g)
                # Save peer information
                json.dump(await self_peer.to_json(), f)
                _logger.debug("Peer information created and saved")
        # Set peer address
        await self_peer.set_address(args.multiaddr)
        # Create bandwidth simulator
        simulator = RateLimitSimulator(12, fixed_latency_bandwidth(0, 12))
        # Create P2P network instace
        network = Network(g, self_peer, None)
        # Start network and log address
        await network.start(args.bootstrap_addr)
        address = await self_peer.get_address()
        _logger.debug("Peer started and listening on {}".format(address))
        # Create engine
        engine = Engine(network)
        # Training configurations
        train_task_config = TRAINING_TASK_CONFIG[args.network]
        backend_config = BACKEND_CONFIG[args.backend]
        backend_config["backend_config"]["device"] = args.device
        # Save training statistics in file
        if args.training_stats:
            training_stats = backend_config["backend_config"]["training_stats"] = {}
            training_stats["training_name"] = args.training_name or args.training_stats
        # Start training task
        _logger.debug("Training started")
        group_id = await engine.publish_task(
            n_workers=args.n_workers,
            **backend_config,
            **train_task_config
        )
        _logger.debug("Training completed")
        # Create testing dataset and loader
        test_dataset_config = TESTING_DATASET_CONFIG[args.network]
        test_dataset_class = as_class(test_dataset_config["dataset_class"])
        test_dataset = test_dataset_class(test_dataset_config["dataset_config"])
        loader = DataLoader(
            dataset=test_dataset,
            batch_size=train_task_config["dataset_config"]["batch_size"]
        )
        # Correct predictions and total tests
        correct = 0
        total = 0
        # Log every n test batches
        LOG_EVERY_N_BATCH = 5
        # Apply to task
        for i, (inputs, labels) in enumerate(loader):
            outputs = await engine.apply(group_id, inputs)
            _, predicted = torch.max(outputs, 1)
            # Update correct predictions and total tests
            correct += (predicted==labels).sum()
            total += labels.size(0)
            # Log current batch
            if i%LOG_EVERY_N_BATCH==0:
                _logger.debug("Test complete for batch #{}".format(i))
        # Accuracy
        accuracy = 100*correct/total
        _logger.info("Accuracy: {}%".format(accuracy))
        # Save accuracy and training statistics
        if args.training_stats:
            training_stats["accuracy"] = accuracy.item()
            with open(args.training_stats, "w") as f:
                json.dump(training_stats, f)
    finally:
        # Stop network
        await network.stop()
        # Kill JS bridge process
        js_process.kill()

if __name__=="__main__":
    exit(main())
