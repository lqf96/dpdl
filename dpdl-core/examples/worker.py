#! /usr/bin/env python3
import asyncio, os, json, logging, sys
from argparse import ArgumentParser

from dpdl.utils.js_bridge import js_bridge_helper
from dpdl.engine import Engine
from dpdl.network import Network, Peer
from dpdl.network.rate_limit import RateLimitSimulator, fixed_latency_bandwidth

## Module logger
_logger = logging.getLogger(__name__)
# Logger level
_logger.setLevel(logging.DEBUG)

def main():
    parser = ArgumentParser()
    # Arguments
    parser.add_argument(
        "-p", "--peer-info",
        help="JSON file for loading or saving peer information.",
        required=True
    )
    parser.add_argument(
        "-a", "--multiaddr",
        help="Multiaddr to listen on.",
        default="/ip4/127.0.0.1/tcp/0"
    )
    parser.add_argument(
        "--bootstrap-addr",
        help="P2P bootstrapping address."
    )
    # Parse arguments
    args = parser.parse_args()
    # Set default event loop
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    # Setup logger
    logging.basicConfig(
        format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s",
        stream=sys.stderr
    )
    # Start function
    loop.run_until_complete(start(args))
    return 0

async def start(args):
    try:
        # Create JS bridge
        js_bridge, js_process = await js_bridge_helper(["dpdl-io"])
        js_bridge.start()
        _logger.debug("JS bridge started")
        # Load or create peer information file
        g = await js_bridge.g()
        if os.path.exists(args.peer_info):
            with open(args.peer_info) as f:
                peer_info_dict = json.load(f)
                self_peer = await Peer.self_existing(js_bridge, peer_info_dict)
                _logger.debug("Peer information loaded")
        else:
            with open(args.peer_info, "w") as f:
                self_peer = await Peer.self_new(g)
                # Save peer information
                json.dump(await self_peer.to_json(), f)
                _logger.debug("Peer information created and saved")
        # Set peer address
        await self_peer.set_address(args.multiaddr)
        # Create bandwidth simulator
        simulator = RateLimitSimulator(12, fixed_latency_bandwidth(0, 12))
        # Create P2P network instace
        network = Network(g, self_peer, None)
        # Start network and log address
        await network.start(args.bootstrap_addr)
        address = await self_peer.get_address()
        _logger.debug("Peer started and listening on {}".format(address))
        # Create engine
        engine = Engine(network)
        # Training task loop
        while True:
            await engine.wait_accept_task()
    finally:
        # Stop network
        await network.stop()
        # Kill JS bridge process
        js_process.kill()

if __name__=="__main__":
    exit(main())
