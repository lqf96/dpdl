#! /usr/bin/env python3
from setuptools import setup

setup(
    name="dpdl",
    version="0.1.0",
    author="Qifan Lu",
    author_email="lqf.1996121@gmail.com",
    url="https://gitlab.com/lqf96/dpdl",
    packages=["dpdl"],
    install_requires=[
        "numpy",
        "torch",
        "torchvision",
        "msgpack",
        "msgpack-numpy",
        "rx",
        "tsp_solver2"
    ],
    test_suite="dpdl_tests.test_suite"
)
