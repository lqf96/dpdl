#! /usr/bin/env node
let {Socket} = require("net");
let {ArgumentParser} = require("argparse");

let {JSBridge, IOHandler} = require("../src/js-bridge");

parser = new ArgumentParser({
    version: "0.1.0",
    addHelp: true,
    description: "DPDL JS bridge testing process."
});
// JS bridge arguments
parser.addArgument(["-s", "--sock-fd"], {
    help: "Socket file descriptor.",
    type: Number,
    required: true
});
// Parse arguments
let args = parser.parseArgs();

// JS bridge test type
class JSBridgeTest {
    constructor(bool_val, test_func) {
        // Test boolean value
        this.bool_val = bool_val;
        // Test function
        this.test_func = test_func;
    }

    /**
     * Test member method calling.
     */
    test_member_method(str) {
        return Buffer.from(`${this.bool_val} ${str}`);
    }

    /**
     * Test invoking Python callback function.
     */
    test_invoke_py_callback(py_func) {
        setImmediate(() => {
            py_func(this.test_func);
        });
    }
}

// JS bridge test global scope
let js_global_scope = {
    "Array": Array,
    "Object": Object,
    "JSBridgeTest": JSBridgeTest,
    "test_prop_1": 42,
    "test_func_1": (num) => num+42,
    "test_func_2": () => { throw new Error("Test error"); }
};

// Create JS bridge
let js_bridge = new JSBridge(
    js_global_scope,
    new Socket({fd: args.sock_fd})
);
// Start JS bridge
js_bridge.start();
