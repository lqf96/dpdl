#! /usr/bin/env node
let fs = require("fs");
let util = require("util");
let {Socket} = require("net");
let {ArgumentParser} = require("argparse");

let {DPDLNode, pipe_from, pipe_to} = require("./node");
let {JSBridge, IOHandler} = require("./js-bridge");

parser = new ArgumentParser({
    version: "0.1.0",
    addHelp: true,
    description: "DPDL IO process."
});
// JS bridge arguments
parser.addArgument(["-s", "--sock-fd"], {
    help: "Socket file descriptor.",
    type: Number,
    required: true
});
// Parse arguments
let args = parser.parseArgs();

// JS global scope
let js_global_scope = {
    "Array": Array,
    "Object": Object,
    "PeerInfo": require("peer-info"),
    "PeerId": require("peer-id"),
    "DPDLNode": DPDLNode,
    "pipe_from": pipe_from,
    "pipe_to": pipe_to
};

// Create JS bridge
let js_bridge = new JSBridge(
    js_global_scope,
    new Socket({fd: args.sock_fd})
);
// Start JS bridge
js_bridge.start();
