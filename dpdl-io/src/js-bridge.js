let weak = require("weak");
let msgpack = require("msgpack-lite");

let utils = require("./utils");

// Remove object
const JSB_REMOVE_OBJ = "remove_obj";
// Call function
const JSB_CALL_FUNC = "call_func";

// Default basic types
const DEFAULT_BASIC_TYPES = [
    Number,
    String,
    Boolean,
    Buffer
];

// Python object proxy handlers
const PY_PROXY_HANDLERS = {
    // Handle function call
    async apply(target, this_arg, args) {
        let js_bridge = target.js_bridge;
        // Arguments
        let func_args = args.map(js_bridge._as_handle.bind(js_bridge));

        // Request function call
        let resp = await js_bridge._request(JSB_CALL_FUNC, {
            "obj": target.handle,
            "args": func_args
        });

        // Unwrap handle
        return js_bridge._unwrap_handle(resp.result);
    },
    // Handle destruction
    _destruct(target) {
        // JS bridge
        let js_bridge = target.js_bridge;
        
        // Remove object from mapping
        js_bridge._py_obj_map.delete(target.handle.py_obj_id);
        // Request object removal
        js_bridge._request(JSB_REMOVE_OBJ, {
            "obj": target.handle
        });
    }
};

/**
 * Create a new Python object proxy.
 * 
 * @param {JSBridge} js_bridge - JS bridge object.
 * @param {PyHandle} handle - Python handle object.
 */
function py_proxy(js_bridge, handle) {
    // Target object
    let target = {
        "js_bridge": js_bridge,
        "handle": handle
    };
    // In order to make proxy callable, its target must be a function
    let target_func = Object.assign(() => null, target);

    // Create proxy
    let proxy = new Proxy(target_func, PY_PROXY_HANDLERS);
    // Create weak reference to proxy
    let weak_proxy = weak(proxy, () => PY_PROXY_HANDLERS._destruct(target));

    return [proxy, weak_proxy];
}

/**
 * Python object handle class.
 */
class PyHandle {
    constructor(py_obj_id) {
        // Python object ID
        this.py_obj_id = py_obj_id;
    }

    static serialize(handle) {
        return msgpack.encode({
            "py_obj_id": handle.py_obj_id
        });
    }

    static deserialize(buffer) {
        let data = msgpack.decode(buffer);
        // Create instance
        return new PyHandle(data["py_obj_id"]);
    }
}

/**
 * JS object handle class.
 */
class JSHandle {
    constructor(js_obj_id) {
        // JS object ID
        this.js_obj_id = js_obj_id;
    }

    static serialize(handle) {
        return msgpack.encode({
            "js_obj_id": handle.js_obj_id
        });
    }

    static deserialize(buffer) {
        let data = msgpack.decode(buffer);
        // Create instance
        return new JSHandle(data["js_obj_id"]);
    }
}

// MessagePack codec
let codec = msgpack.createCodec();
// JSHandle
codec.addExtPacker(0x30, JSHandle, JSHandle.serialize);
codec.addExtUnpacker(0x30, JSHandle.deserialize);
// PyHandle
codec.addExtPacker(0x31, PyHandle, PyHandle.serialize);
codec.addExtUnpacker(0x31, PyHandle.deserialize);

/**
 * JavaScript calling bridge class.
 */
class JSBridge {
    constructor(global_scope, sock, basic_types=[]) {
        // Global scope
        this.global_scope = global_scope;
        // Basic types
        this.basic_types = new Set(DEFAULT_BASIC_TYPES.concat(basic_types));
        // Socket
        this.sock = sock;

        // JS object mapping
        this._js_obj_map = new Map();
        // JS object reverse mapping
        this._js_obj_rmap = new Map();
        // JS object count
        this._js_obj_count = 0;

        // Python object mapping
        this._py_obj_map = new Map();
        // Python object reverse mapping
        this._py_obj_rmap = new WeakMap();

        // Request ID count
        this._req_id_count = 0;
        // Request table
        this._req_table = new Map();
    }

    start() {
        // Message length
        let msg_len = null;

        // Handle incoming data
        this.sock.on("data", utils.read_helper(async (read) => {
            // Read cycle
            while (true) {
                // Message length
                let msg_len_buf = await read(4);
                let msg_len = msg_len_buf.readUInt32LE(0);
                // Message
                let msg_data = await read(msg_len);

                // Handle message
                this._handle_msg(msg_data);
            }
        }));
    }

    async _handle_msg(raw_data) {
        // Decode raw data
        let msg = msgpack.decode(raw_data, {codec: codec});
        
        // Handle Python request
        if ("py_req_id" in msg)
            await this._handle_py_request(msg);
        // Resolve JS request promise
        else {
            this._req_table.get(msg.js_req_id)(msg);
            this._req_table.delete(msg.js_req_id);
        }
    }

    async _handle_py_request(msg) {
        // Handle message with corresponding handler
        let handler = this["_js_"+msg["msg_type"]];
        let resp = handler.call(this, msg);
        // Add message type and request ID
        resp["py_req_id"] = msg["py_req_id"]
        resp["msg_type"] = msg["msg_type"]
        // Encode response
        let raw_data = msgpack.encode(resp, {codec: codec});
        
        // Response length
        let len_buf = Buffer.alloc(4);
        len_buf.writeUInt32LE(raw_data.length);
        // Write response
        let resp_buf = Buffer.concat([len_buf, raw_data]);
        this.sock.write(resp_buf);
    }

    _as_handle(obj) {
        // Null value
        if (obj==null)
            return obj;
        // Basic types
        if (this.basic_types.has(obj.constructor))
            return obj;
        
        let py_obj_id = this._py_obj_rmap.get(obj);
        // Wrap as Python handle
        if (py_obj_id!=null)
            return new PyHandle(py_obj_id);

        let js_obj_id = this._js_obj_rmap.get(obj);
        // Add object to JS object mappings
        if (js_obj_id==null) {
            js_obj_id = this._js_obj_count;
            this._js_obj_count++;

            this._js_obj_map.set(js_obj_id, obj);
            this._js_obj_rmap.set(obj, js_obj_id);
        }
        // Wrap as JS handle
        return new JSHandle(js_obj_id);
    }

    _unwrap_handle(handle) {
        // Unwrap JS handle
        if (handle instanceof JSHandle)
            return this._js_obj_map.get(handle.js_obj_id);

        // Unwrap Python handle
        if (handle instanceof PyHandle) {
            let py_obj_map = this._py_obj_map;
            // Python object ID
            let py_obj_id = handle.py_obj_id;

            // Existing Python object
            if (py_obj_map.has(py_obj_id))
                return weak.get(py_obj_map.get(py_obj_id));

            // Create Python object proxy
            let [proxy, weak_proxy] = py_proxy(this, handle);
            // Add weak proxy to Python object mappings
            py_obj_map.set(py_obj_id, weak_proxy);
            this._py_obj_rmap.set(proxy, py_obj_id);

            return proxy;
        }

        // Basic types
        return handle;
    }

    async _request(msg_type, msg={}) {
        // Request ID
        let req_id = this._req_id_count;
        this._req_id_count++;
        // Add message type and request ID
        msg["msg_type"] = msg_type;
        msg["js_req_id"] = req_id;

        // Make a promise and save it in request table
        let req_promise = new Promise((resolve, reject) => {
            this._req_table.set(req_id, resolve);
        });
        // Encode request
        let raw_data = msgpack.encode(msg, {codec: codec})
        // Request length
        let len_buf = Buffer.alloc(4);
        len_buf.writeUInt32LE(raw_data.length);
        // Write request
        let req_buf = Buffer.concat([len_buf, raw_data]);
        this.sock.write(req_buf);
        
        // Wait for the response
        return req_promise;
    }

    _js_global_scope(args) {
        // Global scope ID
        let global_scope = this._as_handle(this.global_scope);

        return {"value": global_scope};
    }

    _js_remove_obj(args) {
        // Get object and ID
        let js_obj_id = args.obj.js_obj_id;
        let obj = this._unwrap_handle(args.obj);
        // Remove object from two mappings
        this._js_obj_map.delete(js_obj_id);
        this._js_obj_rmap.delete(obj);

        return {};
    }

    _js_get_prop(args) {
        let obj = this._unwrap_handle(args.obj);
        // Key
        let key = this._unwrap_handle(args.key);
        // Get property
        let value = this._as_handle(obj[key]);
        
        return {"value": value};
    }

    _js_set_prop(args) {
        let obj = this._unwrap_handle(args.obj);
        // Key
        let key = this._unwrap_handle(args.key);
        // Value
        let value = this._unwrap_handle(args.value);
        // Set property
        obj[key] = value;

        return {};
    }

    _js_call_member(args) {
        let obj = this._unwrap_handle(args.obj);
        // Member key
        let member_key = this._unwrap_handle(args.member_key);
        // Arguments
        let func_args = args.args.map(this._unwrap_handle.bind(this));
        
        // Call member function
        let result = null;
        try {
            result = obj[member_key].apply(obj, func_args);
            result = this._as_handle(result);
            // Return result
            return {"result": result};
        } catch (error) {
            // Return exception
            return {
                "exception": this._as_handle(error),
                "message": error.toString(),
                "stack": error.stack
            };
        }
    }

    _js_call_func(args) {
        let obj = this._unwrap_handle(args.obj);
        // Arguments
        let func_args = args.args.map(this._unwrap_handle.bind(this));
        
        // Call function
        let result = null;
        try {
            result = this._as_handle(obj(...func_args));
            // Return result
            return {"result": result};
        } catch (error) {
            // Return exception
            return {
                "exception": this._as_handle(error),
                "message": error.toString(),
                "stack": error.stack
            };
        }
    }

    _js_new_obj(args) {
        let obj = this._unwrap_handle(args.obj);
        // Arguments
        let func_args = args.args.map(this._unwrap_handle.bind(this));

        // Create instance
        let result = null;
        try {
            result = this._as_handle(new obj(...func_args));
        } catch (error) {
            // Return exception
            return {
                "exception": this._as_handle(error),
                "message": error.toString(),
                "stack": error.stack
            };
        }

        // Return result
        return {"result": result};
    }
}

// Exports
exports.JSBridge = JSBridge;
