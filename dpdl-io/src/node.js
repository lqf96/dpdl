let LibP2P = require("libp2p");
let TCPTransport = require("libp2p-tcp");
let KadDHT = require("libp2p-kad-dht");
let Multiplex = require("libp2p-mplex");
let SecIO = require("libp2p-secio");
let Railing = require("libp2p-railing");
let pull = require("pull-stream");

/**
 * DPDL node class.
 */
class DPDLNode extends LibP2P {
    /**
     * DPDL node constructor
     * 
     * @param {PeerInfo} peer_info - Peer information
     * @param {string} bootstrap_addr - Bootstrap Multiaddr
     */
    constructor(peer_info, bootstrap_addr=null) {
        // Libp2p configurations
        let modules = {
            // TCP transport only
            transport: [new TCPTransport()],
            // Connection
            connection: {
                // Connection multiplexer
                muxer: [Multiplex],
                // Security
                crypto: [SecIO]
            },
            // DHT provider
            DHT: KadDHT
        };
        // Discovery
        if (bootstrap_addr)
            modules.discovery = [new Railing([bootstrap_addr])];

        super(modules, peer_info);
    }
}

function pipe_from(source, conn) {
    // Add an arrow function to avoid problems caused by proxy
    pull(
        (end, cb) => { source(end, cb); },
        conn
    );
}

function pipe_to(conn, target) {
    // Add an arrow function to avoid problems caused by proxy
    pull(conn, (read) => {
        // Read from stream
        read(null, function next(end, data) {
            // Call target function
            target(end, data);
            // End of stream
            if (end==true)
                return;
            
            setImmediate(() => read(null, next));
        });
    });
}

// Exports
exports.DPDLNode = DPDLNode;
exports.pipe_from = pipe_from;
exports.pipe_to = pipe_to;
