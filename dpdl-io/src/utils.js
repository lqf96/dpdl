function callback_to_promise() {
    // Resolve and reject function
    let resolve_func = null;
    let reject_func = null;

    // Callback
    function callback(err, ...result) {
        // Error
        if (err)
            reject_func(err);
        // Result
        else {
            if (result.length==1)
                result = result[0];
            resolve_func(result);
        }
    }
    // Promise
    let promise = new Promise((resolve, reject) => {
        resolve_func = resolve;
        reject_func = reject;
    });

    return [promise, callback];
}

/**
 * Generate a random integer in given range.
 * 
 * @param {Number} range_start - Range start (included)
 * @param {*} range_end - Range end (Not included)
 */
function rand_int(range_start, range_end) {
    let range = range_end-range_start;
    return Math.floor(Math.random()*range+range_start);
}

function read_helper(func) {
    // Read request queue
    let read_req = [];
    // Received data
    let recv_data = Buffer.alloc(0);

    /**
     * Asynchronous read function.
     * 
     * @param {Number} size
     */
    function read(size) {
        if (recv_data.length>=size) {
            // Return data if available
            let data = recv_data.slice(0, size);
            // Update received data
            recv_data = recv_data.slice(size);

            return data;
        } else {
            let resolve_func = null;
            // Resolved when data is received
            let promise = new Promise((resolve, _) => {
                resolve_func = resolve;
            });
            // Add to request queue
            read_req.push([size, resolve_func]);

            return promise;
        }
    }
    // Invoke function with read helper function
    func(read);

    // Data received handler
    return (data) => {
        // Update received data
        recv_data = Buffer.concat([recv_data, data]);
        // Offset
        let offset = 0;

        while (read_req.length>0) {
            // Process next read request
            let [size, resolve_func] = read_req[0];

            // Fully received
            if (offset+size<=recv_data.length) {
                let data = recv_data.slice(offset, offset+size);
                // Resolve promise
                resolve_func(data);

                // Update offset
                offset += size;
                // Remove request from queue
                read_req.shift();
            } else {
                // Not fully received
                break;
            }
        }
        // Remove received data
        recv_data = recv_data.slice(offset);
    };
}

function delay(timeout) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, timeout);
    });
}

// Exports
exports.callback_to_promise = callback_to_promise;
exports.rand_int = rand_int;
exports.read_helper = read_helper;
exports.delay = delay;
