let assert = require("assert");
let PeerId = require("peer-id");
let PeerInfo = require("peer-info");
let to_stream = require('pull-stream-to-stream')

let {DPDLNode, pipe_from, pipe_to} = require("../src/node");
let {callback_to_promise, rand_int, delay} = require("../src/utils");

describe("Network", function() {
    // PeerId creation test
    it("should be able to create a new PeerId", async function() {
        this.timeout(8000);

        let [p, cb] = callback_to_promise();
        // PeerId 1
        PeerId.create(cb);
        this.peer_id_1 = await p;
    });

    // PeerId loading test
    it("should be able to load existing PeerId", async function() {
        this.timeout(8000);

        let [p1, cb1] = callback_to_promise();
        //PeerId 2
        PeerId.create(cb1);
        let peer_id_2 = await p1;

        // Convert PeerId to JSON
        let peer_id_info = peer_id_2.toJSON();
        // Load PeerId
        let [p2, cb2] = callback_to_promise();
        PeerId.createFromJSON(peer_id_info, cb2);
        this.peer_id_2 = await p2;
    });

    // Add Multiaddr test
    it("should be able to add Multiaddr for a peer", function() {
        // Create PeerInfo from PeerId
        let peer_info_1 = this.peer_info_1 = new PeerInfo(this.peer_id_1);
        // Pick random port
        let port_1 = this.port_1 = rand_int(32768, 49152);
        console.log(`Pick port for peer 1: ${port_1}`);
        // Add a Multiaddr
        peer_info_1.multiaddrs.add(`/ip4/127.0.0.1/tcp/${port_1}`);
    });

    // Get Multiaddr test
    it("should be able to get the Multiaddr of a peer", function() {
        // Get Multiaddr array
        let multiaddrs = this.peer_info_1.multiaddrs.toArray();
        // Get Multiaddr
        let multiaddr = multiaddrs[0].toString();
        assert.equal(multiaddr, `/ip4/127.0.0.1/tcp/${this.port_1}`);
    });

    // Libp2p nodes creation test
    it("should be able to create libp2p nodes", async function() {
        // Create peer 1
        let node_1 = this.node_1 = new DPDLNode(this.peer_info_1);
        
        // Create PeerInfo 2
        let peer_info_2 = this.peer_info_2 = new PeerInfo(this.peer_id_2);
        // Pick random port
        let port_2 = rand_int(32768, 49152);
        console.log(`Pick port for peer 2: ${port_2}`);
        // Add a Multiaddr
        peer_info_2.multiaddrs.add(`/ip4/127.0.0.1/tcp/${port_2}`);
        // Create peer 2
        let node_2 = this.node_2 = new DPDLNode(peer_info_2);

        // Start node 1
        let [p1, cb1] = callback_to_promise();
        node_1.start(cb1)
        await p1;
        // Start node 2
        let [p2, cb2] = callback_to_promise();
        node_2.start(cb2)
        await p2;
    });

    // Node connection test
    it("should be able to connect to another node", async function() {
        const TEST_PROTOCOL = "/test/0.1.0"

        // Handle protocol for peer 2
        this.node_2.handle(TEST_PROTOCOL, (_, conn) => {
            this.conn_2 = conn;
        });
        // Connect from peer 1 to peer 2
        let [p, cb] = callback_to_promise();
        this.node_1.dialProtocol(this.peer_info_2, TEST_PROTOCOL, cb);
        this.conn_1 = await p;
    });

    // Sending and receiving test
    it("should be able to send and receive data on the connection", async function() {
        const TEST_DATA_1 = Buffer.from("Test data from peer 1 to peer 2.");
        const TEST_DATA_2 = Buffer.from("Test data from peer 2 to peer 1.");

        // Receive data promise
        let recv_promise_1 = new Promise((resolve, _) => {
            pipe_to(this.conn_2, (_, data) => {
                if (data) {
                    assert(data.equals(TEST_DATA_1));
                    resolve();
                }
            });
        });
        let recv_promise_2 = new Promise((resolve, _) => {
            pipe_to(this.conn_1, (_, data) => {
                if (data) {
                    assert(data.equals(TEST_DATA_2));
                    resolve();
                }
            });
        });

        let flag_1 = false;
        let flag_2 = false;
        // Send data from peer 1 to peer 2
        pipe_from((_, cb) => {
            // Send data or end connection
            if (!flag_1) {
                cb(null, TEST_DATA_1);
                flag_1 = true
            } else
                cb(true);
        }, this.conn_1);
        // Send data from peer 2 to peer 1
        pipe_from((_, cb) => {
            let flag = false;

            // Send data or end connection
            if (!flag_2) {
                cb(null, TEST_DATA_2);
                flag_2 = true;
            } else
                cb(true);
        }, this.conn_2);
        
        await recv_promise_1;
        await recv_promise_2;
    });

    // PubSub test
    it("should be able to send and receive broadcast message", async function() {
        this.timeout(8000);

        const BROADCAST_DATA = Buffer.from("Broadcast test data.")
        // Broadcast received promise
        let resolve_func = null;
        let data_recv_promise = new Promise((resolve, _) => {
            resolve_func = resolve;
        });

        // Subscribe to topic
        let [p1, cb1] = callback_to_promise();
        this.node_1.pubsub.subscribe("dpdl-test", (msg) => {
            assert(msg.data.equals(BROADCAST_DATA));
            resolve_func();
        }, cb1);
        await p1;
        // Wait for other peers to connect
        await delay(200);
        
        // Publish to topic
        let [p2, cb2] = callback_to_promise();
        this.node_2.pubsub.publish("dpdl-test", BROADCAST_DATA, cb2);
        await p2;
        
        console.log("Subscribed to topic");

        await data_recv_promise;
    });

    after(async function() {
        // Stop node 1
        let [p1, cb1] = callback_to_promise();
        this.node_1.stop(cb1);
        await p1;
        // Stop node 2
        let [p2, cb2] = callback_to_promise();
        this.node_2.stop(cb2);
        await p2;
    });
});
