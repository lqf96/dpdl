#! /usr/bin/env python2.7
# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
import json
from argparse import ArgumentParser
import matplotlib.pyplot as plt
import numpy as np

def moving_average(data, window_size):
    result = []
    for i in range(len(data)):
        window_begin = max(0, i-window_size+1)
        result.append(np.average(data[window_begin:i+1]))
    return result

def process_data(data, data_kind, loss_threshold):
    training_name = data["training_name"]
    # Print accuracy
    if data_kind=="epoch_loss":
        print("[{}] Accuracy: {}%".format(training_name, data["accuracy"]))
    # Data reducer mapping
    data_reducer_mapping = {
        "epoch_loss": np.average,
        "epoch_time": np.max
    }
    data_reducer = data_reducer_mapping[data_kind]
    # Kind data
    kind_data = data[data_kind]
    # Process data
    group_data_iter = zip(*[iter(member_data) for member_data in kind_data.values()])
    processed_data = [data_reducer(group_data) for group_data in group_data_iter]
    # Get relative time
    if data_kind=="epoch_time":
        start_time = processed_data[0]
        for i in range(len(processed_data)):
            processed_data[i] -= start_time
        print("[{}] Total time used: {}s".format(training_name, processed_data[-1]))
    # First epoch whose loss below threshold (Use moving average)
    elif data_kind=="epoch_loss":
        for i, loss in enumerate(moving_average(processed_data, 20)):
            if loss<=loss_threshold:
                break
        print("[{}] First epoch below loss threshold: {}".format(training_name, i))
    # Return processed data
    return training_name, processed_data

def plot(data_kind, processed_datum, file_name):
    # X-axis label
    x_label_mapping = {
        "epoch_loss": "Global Epoches",
        "epoch_time": "Global Epoches"
    }
    # Y-axis label
    y_label_mapping = {
        "epoch_loss": "Loss",
        "epoch_time": "Training Time"
    }
    # Set label
    plt.xlabel(x_label_mapping[data_kind])
    plt.ylabel(y_label_mapping[data_kind])
    # Plot each file
    for data in processed_datum:
        training_name, processed_data = data
        # X-axis
        x_data = range(len(processed_data))
        # Plot
        plt.plot(x_data, processed_data, "-", label=training_name)
    # Legend
    plt.legend()
    # Save image
    if file_name:
        plt.savefig(file_name)
    # Show graph
    plt.show()

def main():
    parser = ArgumentParser()
    # Data kind
    parser.add_argument(
        "-k", "--data-kind",
        help="Kind of data to plot.",
        choices=["epoch_loss", "epoch_time"],
        default="epoch_loss"
    )
    # Loss threshold
    parser.add_argument(
        "-l", "--loss-threshold",
        help="Loss threshold for training process.",
        type=float
    )
    # Output file
    parser.add_argument(
        "-o", "--output",
        help="Output graph path."
    )
    # Files to plot
    parser.add_argument(
        "stat_files",
        metavar="Stat File",
        nargs="+",
        help="Statistic files to plot."
    )
    # Parse arguments
    args = parser.parse_args()
    # Load and process files
    processed_datum = []
    for path in args.stat_files:
        with open(path) as f:
            data = json.load(f)
        processed_datum.append(process_data(data, args.data_kind, args.loss_threshold))
    # Plot
    plot(args.data_kind, processed_datum, args.output)
    return 0

if __name__=="__main__":
    exit(main())
