#! /bin/sh

cd "$(dirname $0)"
# Import common configuration
. ./common.conf
# Run bootstrapper
../dpdl-core/examples/bootstrapper.py -p bootstrapper.json -a "${BOOTSTRAPPER_LISTEN_ADDR}"
