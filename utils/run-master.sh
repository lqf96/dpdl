#! /bin/sh

cd "$(dirname $0)"
# Import common configuration
. ./common.conf
# Run master
../dpdl-core/examples/master.py -p master.json --bootstrap-addr "${BOOTSTRAP_ADDR}" "$@"
