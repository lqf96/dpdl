#! /bin/sh

cd "$(dirname $0)"
# Import common configuration
. ./common.conf
# Start given amount of workers in the background
echo "[Run Workers Script] Starting workers..."
WORKER_PIDS=""
for i in $(seq 1 $1); do
    CUDA_VISIBLE_DEVICES="$i" ../dpdl-core/examples/worker.py -p "worker$i.json" --bootstrap-addr "${BOOTSTRAP_ADDR}" &
    WORKER_PIDS="${WORKER_PIDS} $!"
    sleep 2s
done
# Wait for workers to complete
echo "[Run Workers Script] Enter anything to stop."
read -r STOP_LINE
echo "[Run Workers Script] Killing workers..."
# Kill all Python 3 and Node processes
# TODO: This is a silly workaround; rewrite it some time
killall python3 node
